﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace E_Kitap.Hubs
{
    public class ChatHub : Hub
    {
        public void SendMessage(string name,string message)
        {
            //Clients.All();//tüm clientlera gider
            //Clients.Others();//diğer clientlera gider ben görmem
            //Clients.Caller();//sunucuya gönderiyorum tekrar ben alıyorum sadece bana geliyor.
            //Clients.User("");// sadece burdaki belirttiğim kişiye gider 

            Clients.Others.GetMessageOther(name, message);
            Clients.Caller.GetMessageCaller(message);
           // string id = Context.ConnectionId;
           // Clients.Client(id).GetMessageCaller(message);
           //// Clients.Client(id);
           // Clients.User("5");

 
        }

        public override Task OnConnected()
        {
            string id = Context.ConnectionId;

            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            return base.OnDisconnected(stopCalled); 
        }
    }
}