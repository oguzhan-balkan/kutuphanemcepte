﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.LibraryService;
using E_Kit.Service.Common;
using E_Kitap.Filters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class LibraryController : Controller
    {
        // GET: Library
        private readonly LibraryService _LibraryService = new LibraryService();
        private readonly E_Kit.Service.BranchCategoriesService.BranchCategoriesService _branchCategoriesService = new E_Kit.Service.BranchCategoriesService.BranchCategoriesService();
        private readonly E_Kit.Service.BranchesService.BranchesService _BranchesService = new E_Kit.Service.BranchesService.BranchesService();
        public ActionResult Index()
        {
            ViewBag.AllBranchList = _BranchesService.GetBranchesList();
            return View(_LibraryService.GetLibraryList());
        }
        public ActionResult Create()
        {
           // int i = 0;
           //var list = _LibraryService.GetLibraryList();
           // foreach (var item in list)
           // {
           //     Library model = new Library();
           //     model.BranchCode = item.BranchCode;
           //     model.City = item.City;
           //     model.District = item.District;
           //     model.Id = item.Id;
           //     model.Image = item.Image;
           //     model.ImageUrl = item.ImageUrl;
           //     model.Instagram = "https://www.instagram.com/tckulturturizm/";
           //     model.Twitter = "https://twitter.com/TCKulturTurizm";
           //     model.Youtube = "http://www.youtube.com/user/KulturTurizmBak";
           //     model.Facebook = "https://www.facebook.com/TCKulturTurizm";
           //     Edit(model);
           //     i++;
           //     if (i == list.Count)
           //     {
           //         return RedirectToAction("Index");

           //     }
           // }

            ViewBag.BranchCategoriesList = _branchCategoriesService.GetBranchCategoriesList();
            return View(new Library { IsActive = true });
        }
        [HttpPost]
        public ActionResult Create(Library model)
        {
           
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient(""+ ApiUrlViewModel.Url +"Library");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    
                    using (var ms = new MemoryStream())
                    {
                        model.Image.InputStream.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        var Bytes = ms.Length;
                        if (Bytes >= 1240000)
                        {
                            ViewBag.BranchCategoriesList = _branchCategoriesService.GetBranchCategoriesList();
                            ViewBag.Alert = "Dosya Boyutu Max 1Mb Olmalı";
                            return View(new Library { IsActive = true });


                        }
                        request.AddFile("image", fileBytes, model.Image.FileName);
                    }

                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("BranchCode", model.BranchCode);
                    request.AddParameter("City", model.City);
                    request.AddParameter("District", model.District);
                    request.AddParameter("Facebook", model.Facebook);
                    request.AddParameter("Instagram", model.Instagram);
                    request.AddParameter("Twitter", model.Twitter);
                    request.AddParameter("Youtube", model.Youtube);
                    request.AddParameter("IsActive", true);
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            ViewBag.BranchCategoriesList = _branchCategoriesService.GetBranchCategoriesList();
            var model = new Library();
            model = _LibraryService.FindLibraryById(id);
            return View("Edit",model);
        }

        [HttpPost]
        public ActionResult Edit(Library model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient(""+ApiUrlViewModel.Url+ $"Library?id={model.Id}");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.PUT);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    if (model.Image != null)
                    {
                        using (var ms = new MemoryStream())
                        {
                            model.Image.InputStream.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            request.AddFile("image", fileBytes, model.Image.FileName);
                        }
                    }
                    request.AlwaysMultipartFormData = true;
                     request.AddParameter("IsActive", model.IsActive);
                    request.AddParameter("BranchCode", model.BranchCode);
                    request.AddParameter("City", model.City);
                    request.AddParameter("District", model.District);
                    request.AddParameter("Facebook", model.Facebook);
                    request.AddParameter("Instagram", model.Instagram);
                    request.AddParameter("Twitter", model.Twitter);
                    request.AddParameter("Youtube", model.Youtube);
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }


        public ActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _LibraryService.DeleteLibrary(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}