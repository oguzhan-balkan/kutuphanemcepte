﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.RoleService;
using E_Kit.Service.Common;
using E_Kitap.Filters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class RoleController : Controller
    {
        // GET: Role
        private readonly RoleService _RoleService = new RoleService();
        public ActionResult Index()
        {
            
            return View(_RoleService.GetRoleList());
        }
        public ActionResult Create()
        {
             return View(new Role { IsActive = true });
        }
        [HttpPost]
        public ActionResult Create(Role model)
        {
           
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient(""+ ApiUrlViewModel.Url +"Role");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    

                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("Name", model.Name);
                     request.AddParameter("IsActive", true);
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
 
            return View(_RoleService.FindRoleById(id));
        }

        [HttpPost]
        public ActionResult Edit(Role model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient(""+ApiUrlViewModel.Url+ $"Role?id={model.Id}");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.PUT);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("Name", model.Name);
                     request.AddParameter("IsActive", model.IsActive);
                     IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }


        public ActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _RoleService.DeleteRole(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}