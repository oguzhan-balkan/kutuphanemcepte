﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.ChatUserListService;
using E_Kit.Service.BranchCategoriesService;
using E_Kit.Service.BranchesService;
using E_Kit.Service.Common;
using E_Kitap.Filters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class ChatUserListController : Controller
    {
        // GET: ChatUserList
        private readonly ChatUserListService _ChatUserListService = new ChatUserListService();
        public ActionResult Index()
        {
            
            return View(_ChatUserListService.GetChatUserListList());
        }

        public ActionResult Create()
        {
            
             return View(new ChatUserList { IsActive = true });
        }

        [HttpPost]
        public ActionResult Create(ChatUserList model)
        {
           
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient(""+ ApiUrlViewModel.Url +"ChatUserList");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                   

                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("UserId", model.UserId);
                    request.AddParameter("UserName", model.UserName);
                    request.AddParameter("BranchCode", model.BranchCode);
                    request.AddParameter("BranchName", model.BranchName);
                    request.AddParameter("IsActive", model.IsActive);
                    IRestResponse response = client.Execute(request);
                   var result= JsonConvert.DeserializeObject(response.Content);
                    var user = _ChatUserListService.FindChatUserListById(model.UserId);

                    if (model.IsActive==true && user.IsActive==true)
                    {

                         return Redirect(ApiUrlViewModel.PanelUrl+"Chat/ChatBox?UserId="+model.UserId);
                    }

                    else
                        return RedirectToAction("Index");


                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
        public ActionResult Edit(string id)
        {
             
            return View(_ChatUserListService.FindChatUserListById(id));
        }

        [HttpPost]
        public ActionResult Edit(ChatUserList model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient(""+ApiUrlViewModel.Url+ $"ChatUserList?id={model.Id}");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.PUT);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                   
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("UserId", model.UserId);
                    request.AddParameter("UserName", model.UserName);
                    request.AddParameter("BranchCode", model.BranchCode);
                    request.AddParameter("BranchName", model.BranchName);
                    request.AddParameter("IsActive", true);
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _ChatUserListService.DeleteChatUserList(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}