﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
 using E_Kit.Service.Common;
using E_Kitap.Filters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class CatalogueController : Controller
    {
        private readonly E_Kit.Service.BranchesService.BranchesService _branches = new E_Kit.Service.BranchesService.BranchesService();
        public ActionResult Index()
        {
            ViewBag.BranchesList = _branches.GetBranchesList();
             return View("Index");
        }

        [HttpPost]
        public ActionResult Data(CatalogueSearchViewModel model)
        {
            var data = JsonConvert.SerializeObject(model);

            var client = new RestClient("http://koha-app.ekutuphane.gov.tr/api/catalogue/SearchCatalogue/");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddJsonBody(data);
            request.AddParameter("application/json", "", ParameterType.RequestBody);
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            List<Catalogue> jsonArrayAttribute = JsonConvert.DeserializeObject<List<Catalogue>>(response.Content ?? "");

            ViewBag.CatalogueList = jsonArrayAttribute;
            ViewBag.BranchesList = _branches.GetBranchesList();
            return View("Index");
        }


    }
    

    }