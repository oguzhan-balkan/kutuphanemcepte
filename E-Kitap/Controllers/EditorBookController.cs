﻿using E_Kit.Data.ViewModel;
using E_Kit.Service.BookService;
using E_Kit.Service.Common;
using E_Kitap.Filters;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class EditorBookController : Controller
    {
        // GET: EditorBook
        private readonly EditorBookService _editorBookService = new EditorBookService();
        private readonly BookService _bookService = new BookService();
        // GET: Country
        public ActionResult Index()
        {
            return View(_editorBookService.GetEditorBookList());
        }

        public ActionResult Create()
        {
            ViewBag.Books = new SelectList(_bookService.GetBookSelectList(), "Id", "Name");
            return View(new EditorBookViewModel());
        }
        [HttpPost]
        public ActionResult Create(EditorBookViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _editorBookService.InsertEditorBook(model);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            var model = _editorBookService.FindEditorBookById(id);
            ViewBag.Books = new SelectList(_bookService.GetBookSelectList(), "Id", "Name", model.BookId);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditorBookViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _editorBookService.EditEditorBook(model);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _editorBookService.DeleteEditorBook(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}