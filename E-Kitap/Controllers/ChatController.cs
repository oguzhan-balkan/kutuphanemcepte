﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.Common;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    public class ChatController : Controller
    {
        private readonly E_Kit.Service.ChatUserListService.ChatUserListService _ChatUserListService = new E_Kit.Service.ChatUserListService.ChatUserListService();
        private readonly E_Kit.Service.ChatService.ChatService _ChatService = new E_Kit.Service.ChatService.ChatService();
         // GET: Chat
        public ActionResult Index(string UserId)
        {
            ViewBag.ChatUserList = _ChatUserListService.GetChatUserListList();
             ViewBag.ChatList = _ChatService.GetChatListName(UserId);
            return View();
           
        }

        [HttpGet]
        public JsonResult MessageGet2(string UserId, string ClientId,string BranchCode)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient("" + ApiUrlViewModel.Url + "Chat/GetMessage?UserId=" + UserId + "&ClientId=" + ClientId +"&BranchCode="+BranchCode+ "");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.GET);
                    //request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    //request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    request.AlwaysMultipartFormData = true;
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    var result = response.Content;
                    return Json(new { Data = result }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return Json(new { Data = "false" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult MessageGet(string UserId,string ClientId)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient("" + ApiUrlViewModel.Url + "Chat/GetMessage?UserId="+UserId+"&ClientId="+ClientId+"");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.GET);
                    //request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    //request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    request.AlwaysMultipartFormData = true;
                      IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    var result = response.Content;
                    return Json(new { Data = result }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }

        public void RegisterToken(string platform, string token,string userId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    

                    var client = new RestClient("" + ApiUrlViewModel.Url + "UserMessageToken");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                  
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("Platform", platform);
                    request.AddParameter("Token", token);
                    request.AddParameter("UserId", userId);
                    request.AddParameter("IsActive", true);
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);

                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            
        }

        public ActionResult ChatRoom()
        {
            return View();
        }

        public ActionResult ChatBox(string UserId)
        {
            ViewBag.ChatList = _ChatService.GetChatListName(UserId);

            return View();
        }
    }
}