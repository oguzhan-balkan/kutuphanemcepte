﻿using E_Kit.Data.Entities;
using E_Kit.Service.SettingService;
using E_Kitap.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class SettingController : Controller
    {
        private readonly SettingService _settingService = new SettingService();
        // GET: Setting
        public ActionResult Edit()
        {
            return View(_settingService.GetSetting());
        }

        [HttpPost]
        public ActionResult Edit(Setting model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _settingService.EditSetting(model);
                    return RedirectToAction("Index","Home");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;                
            }
            return View();
        }
    }
}