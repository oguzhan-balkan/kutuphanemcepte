﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.AuthorService;
using E_Kit.Service.Common;
using E_Kitap.Filters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class AuthorController : Controller
    {
        private readonly AuthorService _authorService = new AuthorService();
        // GET: Author
        public ActionResult Index()
        {
            return View(_authorService.GetAuthorList());
        }

        public ActionResult Create()
        {
            return View(new Author { IsActive = true });
        }
        [HttpPost]
        public ActionResult Create(Author model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient("" + ApiUrlViewModel.Url + "Author");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    using (var ms = new MemoryStream())
                    {
                        model.Image.InputStream.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        request.AddFile("Image", fileBytes, model.Image.FileName);
                    }

                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("Name", model.Name);
                    request.AddParameter("LastName", model.LastName);
                    request.AddParameter("Title", model.Title);
                    request.AddParameter("AboutTr", model.AboutTr);
                    request.AddParameter("AboutEn", model.AboutEn);
                    request.AddParameter("IsActive", model.IsActive);
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            return View(_authorService.FindAuthorById(id));
        }

        [HttpPost]
        public ActionResult Edit(Author model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient("" + ApiUrlViewModel.Url + $"Author?id={model.Id}");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.PUT);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    if (model.Image != null)
                    {
                        using (var ms = new MemoryStream())
                        {
                            model.Image.InputStream.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            request.AddFile("Image", fileBytes, model.Image.FileName);
                        }
                    }
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("Name", model.Name);
                    request.AddParameter("LastName", model.LastName);
                    request.AddParameter("Title", model.Title);
                    request.AddParameter("AboutTr", model.AboutTr);
                    request.AddParameter("AboutEn", model.AboutEn);
                    request.AddParameter("IsActive", model.IsActive);
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _authorService.DeleteAuthor(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}