﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.Common;
using E_Kitap.Filters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class BranchesController : Controller
    {
        // GET: Branches
        private readonly E_Kit.Service.BranchesService.BranchesService _BranchesService = new E_Kit.Service.BranchesService.BranchesService();
        public ActionResult Index()
        {
            return View(_BranchesService.GetBranchesList());
        }
       
       
    }
}