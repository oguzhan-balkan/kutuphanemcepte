﻿using E_Kit.Data.Entities;
using E_Kit.Service.TagService;
using E_Kitap.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class TagController : Controller
    {
        // GET: Tag
        private readonly TagService _tagService = new TagService();
        public ActionResult Index()
        {
            return View(_tagService.GetTagList());
        }
        public ActionResult Create()
        {
            return View(new Tags { IsActive = true });
        }
        [HttpPost]
        public ActionResult Create(Tags model, string favcolor)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.ColorCode = favcolor;
                    _tagService.InsertTag(model);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;

            }
            return View();
        }
        public ActionResult Edit(int id)
        {
        
            return View(_tagService.FindTagById(id));
        }

        [HttpPost]
        public ActionResult Edit(Tags model, string favcolor)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.ColorCode = favcolor;
                    _tagService.EditTag(model);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;               
            }
            return View();
        }


        public ActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _tagService.DeleteTag(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}