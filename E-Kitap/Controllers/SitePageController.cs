﻿using E_Kit.Data.Entities;
using E_Kit.Service.SitePageService;
using E_Kitap.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class SitePageController : Controller
    {
        private readonly AboutApplicationService _aboutApplicationService = new AboutApplicationService();
        private readonly PrivacyPolicyService _privacyPolicyService = new PrivacyPolicyService();
        private readonly ContactService _contactService = new ContactService();
        // GET: SitePage
        
        public ActionResult AboutApplication()
        {
            return View(_aboutApplicationService.GetAboutApplication());
        }
        [HttpPost]
        public ActionResult AboutApplication(AboutApplication model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _aboutApplicationService.EditAboutApplication(model);
                    return RedirectToAction("AboutApplication");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
        public ActionResult PrivacyPolicy()
        {
            return View(_privacyPolicyService.GetPrivacyPolicy());
        }
        [HttpPost]
        public ActionResult PrivacyPolicy(PrivacyPolicy model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _privacyPolicyService.EditPrivacyPolicy(model);
                    return RedirectToAction("PrivacyPolicy");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
        public ActionResult Contact()
        {
            return View(_contactService.GetContact());
        }
        [HttpPost]
        public ActionResult Contact(Contact model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _contactService.EditContact(model);
                    return RedirectToAction("Contact");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
    }
}