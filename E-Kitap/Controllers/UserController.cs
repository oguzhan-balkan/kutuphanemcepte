﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.BranchCategoriesService;
using E_Kit.Service.BranchesService;
using E_Kit.Service.RoleService;
using E_Kit.Service.Common;
using E_Kit.Service.UserService;
using E_Kitap.Filters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_Kit.Service.UserRoleService;
  

namespace E_Kitap.Controllers
{
    
    [Auth]
    public class UserController : Controller
    {
        private readonly UserService _userService = new UserService();
        private readonly BranchCategoriesService _branchCategoriesService = new BranchCategoriesService();
        private readonly BranchesService _BranchesService = new BranchesService();
        private readonly RoleService  _roleService = new RoleService();
        private readonly UserRoleService  _userRoleService = new UserRoleService();
        public ActionResult Index(string id)
        {
            
            return View(_userService.FindByUserId(id));
        }


        public ActionResult UserList()
        {
            return View(_userService.GetUserList());
        }

        public ActionResult Create()
        {
            ViewBag.BranchCategoriesList = _branchCategoriesService.GetBranchCategoriesList();
            ViewBag.RoleList = _roleService.GetRoleList();

            return View(new User { IsActive = true });
        }

        public ActionResult Edit(string id)
        {
            ViewBag.BranchCategoriesList = _branchCategoriesService.GetBranchCategoriesList();
            ViewBag.RoleList = _roleService.GetRoleList();
            return View(_userService.FindByUserId(id));
        }


        [HttpPost]
        public ActionResult Edit(User model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient("" + ApiUrlViewModel.Url + $"User?id={model.Id}");

                     client.Timeout = -1;
                    var request = new RestRequest(Method.PUT);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());

                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("Email", model.Email);
                    request.AddParameter("Name", model.Name);
                    request.AddParameter("LastName", model.LastName);
                    request.AddParameter("PhoneNumber", model.PhoneNumber);
                    request.AddParameter("UserGroupId", model.UserGroupId);
                    request.AddParameter("BranchName", model.BranchName);
                    request.AddParameter("BranchCode", model.BranchCode);
                    request.AddParameter("BranchCity", model.BranchCity);
                    request.AddParameter("RoleId", model.RoleId);
                    request.AddParameter("IsActive", true);
                    IRestResponse response = client.Execute(request);
                  
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("UserList");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(User model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient("" + ApiUrlViewModel.Url + "User");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
 
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("Email", model.Email);
                    request.AddParameter("Name", model.Name);
                    request.AddParameter("LastName", model.LastName);
                    request.AddParameter("PhoneNumber", model.PhoneNumber);
                    request.AddParameter("UserGroupId", model.UserGroupId);
                    request.AddParameter("BranchName", model.BranchName);
                    request.AddParameter("BranchCode", model.BranchCode);
                    request.AddParameter("BranchCity", model.BranchCity);
                    request.AddParameter("RoleId", model.RoleId);
                    request.AddParameter("CardNumber", model.CardNumber);
                    request.AddParameter("IsActive", true);
                    IRestResponse response = client.Execute(request);
                     
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("UserList");                  
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }

        public ActionResult ChangePassword(string id)
        {
            ChangePasswordViewModel model = new ChangePasswordViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _userService.ChangePassword(model);
                    return RedirectToAction("Index", new { id = model.Id });
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }

        public ActionResult PasswordForgot(string email)
        {
            ChangePasswordViewModel model = new ChangePasswordViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult ForgotPassword(string email)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _userService.ForgotPassword(email);
                    return RedirectToAction("Index", new { email=email });
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }

        public ActionResult Delete(string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _userService.DeleteUser(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("UserList");
            }
            return RedirectToAction("UserList");
        }

    }
}