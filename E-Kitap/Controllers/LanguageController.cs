﻿using E_Kit.Data.Entities;
using E_Kit.Service.LanguageService;
using E_Kitap.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class LanguageController : Controller
    {
        // GET: Language
        private readonly LanguageService _languageService = new LanguageService();
        public ActionResult Index()
        {
            return View(_languageService.GetLanguageList());
        }
        public ActionResult Create()
        {
            return View(new Language { IsActive = true });
        }
        [HttpPost]
        public ActionResult Create(Language model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _languageService.InsertLanguage(model);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            return View(_languageService.FindLanguageById(id));
        }

        [HttpPost]
        public ActionResult Edit(Language model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _languageService.EditLanguage(model);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _languageService.DeleteLanguage(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}