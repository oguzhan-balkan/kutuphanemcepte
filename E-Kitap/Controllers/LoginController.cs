﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using E_Kit.Data.Entities;
using E_Kit.Data.Entities.Enums;
using E_Kit.Data.ViewModel;
using E_Kit.Service.Common;
using E_Kit.Service.UserRoleService;
using E_Kit.Service.UserService;
using Newtonsoft.Json;
using RestSharp;

namespace E_Kitap.Controllers
{
    public class LoginController : Controller
    {
        private readonly UserService _userService = new UserService();
        private readonly UserRoleService _userRoleService = new UserRoleService();
        private readonly E_Kit.Service.BranchesService.BranchesService _BranchesService = new E_Kit.Service.BranchesService.BranchesService();

        public ActionResult Index()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        public ActionResult Index(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseLoginViewModel.Email = model.Email;
                ResponseLoginViewModel.Password = model.Password;
                var userModel = _userService.Login(model.Email, model.Password);
                if (userModel != null)
                {                
                    ResponseLoginViewModel.Token = userModel.Auth_token;
                    Session["login"] = userModel;
                    Session["User"] = _userService.FindByUserId(userModel.Id);
                    //var role = _userRoleService.FindUserRoleById(userModel.Id);
                    //User user = new User();
                    //user.RoleId = role.RoleId;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.Message = "Yetkiniz bulunmamaktadır.";
                }
            }
            return View();
        }


        [HttpPost]
        public ActionResult KohaLogin(KohaLoginViewModel model)
        {

            var client = new RestClient("" + ApiUrlViewModel.OtherUrl + "api/borrower/BorrowerLogin");//kullanıcı on login durumu
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);


            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(model);
            request.AddBody(model);
             
            request.AddParameter("application/json", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
        var sonuc=    JsonConvert.DeserializeObject<KohaOnLogin>(response.Content);
            if (sonuc.OnLogin == true)
            {
                var client2 = new RestClient("" + ApiUrlViewModel.OtherUrl + "api/token/gettoken");// static token alma işlemi (saçma bi yöntem!!)
                client2.Timeout = -1;
                var request2 = new RestRequest(Method.POST);
                request2.AddHeader("Content-Type", "application/json");
                var body = @"{
" + "\n" +
                @"    ""username"":""43324834562"",
" + "\n" +
                @"    ""password"":""Twain@2020!"",
" + "\n" +
                @"    ""userguid"":""0b477c7a-5875-4bb8-89bf-45aa651705f8""
" + "\n" +
                @"}";
                request2.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response2 = client2.Execute(request2);


                var client3 = new RestClient("" + ApiUrlViewModel.OtherUrl + "/api/borrower/getborrowerDetail/" + model.cardnumber + "");//kullanıcı detay bilgilerini getir
                var request3 = new RestRequest(Method.GET);
                //List<KohaTokenModel> jsonArrayAttribute = JsonConvert.DeserializeObject<List<KohaTokenModel>>(response2.Content ?? "");
                var jsonArrayAttribute = JsonConvert.DeserializeObject<KohaTokenModel.KohaTokenData>(response2.Content);
                request3.AddHeader("Authorization", "Bearer " + jsonArrayAttribute.token + "");
                IRestResponse response3 = client3.Execute(request3);
                var jsonUser = JsonConvert.DeserializeObject<KohaUser>(response3.Content);
                if (jsonUser.Borrower.Categorycode!="E")
                {
                    ViewBag.Message = "Yetkiniz bulunmamaktadır.";
                    return RedirectToAction("KohaLogin", "Login");
                }
                var userList = _userService.GetUserList();
                User userInfoModel = new User();

                userInfoModel = userList.Where(p => p.CardNumber == jsonUser.Borrower.Cardnumber).FirstOrDefault();// tc ye göre kullanıcı bizim db de var mı 
                if (userInfoModel != null)
                {
                    User userModel = new User();
                    userModel.Id = userInfoModel.Id;
                    userModel.BranchCode = jsonUser.Borrower.Branchcode.ToString();
                    userModel.Name = jsonUser.Borrower.Firstname;
                    userModel.LastName = jsonUser.Borrower.Surname;
                    string branchCode = jsonUser.Borrower.Branchcode.ToString();
                    var branch = _BranchesService.GetBranchesList();
                    var x = branch.Where(p => p.branchcode == branchCode).FirstOrDefault();

                    var email1 = "";
                    if (jsonUser.Borrower.Email == null)
                    {
                        jsonUser.Borrower.Email = "";

                    }
                    string boslukkontrol = (jsonUser.Borrower.Email.TrimStart()).TrimEnd();//boşluklarıalmak için
                    if (boslukkontrol != "")
                    {
                        email1 = jsonUser.Borrower.Email;
                    }
                    else
                    {

                        var sayi = jsonUser.Borrower.Cardnumber;

                        email1 = sayi + "admin@mail.com";
                    }
                    userModel.PhoneNumber = jsonUser.Borrower.Phonepro;
                    userModel.CardNumber = jsonUser.Borrower.Cardnumber;
                    userModel.BranchName = x.branchname;
                    userModel.BranchCity = jsonUser.Borrower.City;
                    userModel.Email = email1;
                    userModel.RoleId = 2;
                    userModel.UserGroupId = 2;
                    UserEdit(userModel);
                    var email = "";
                    if (jsonUser.Borrower.Email == null)
                    {
                        jsonUser.Borrower.Email = "";

                    }
                    string boslukkontrol2 = (jsonUser.Borrower.Email.TrimStart()).TrimEnd();//boşluklarıalmak için
                    if (boslukkontrol2 != "")
                    {
                        email = jsonUser.Borrower.Email;
                    }
                    else
                    {

                        var sayi = jsonUser.Borrower.Cardnumber;

                        email = sayi + "admin@mail.com";
                    }
                    ResponseLoginViewModel.Password = "E-kitap123456e";
                    LoginViewModel LoginModel = new LoginViewModel();
                    LoginModel.Email = email;
                    LoginModel.Password = "E-kitap123456e";

                    var userModel2 = _userService.Login(LoginModel.Email, LoginModel.Password);
                    if (userModel2 != null)
                    {
                        ResponseLoginViewModel.Token = userModel2.Auth_token;
                        Session["login"] = userModel2;
                        Session["User"] = _userService.FindByUserId(userModel2.Id);
                        //var role = _userRoleService.FindUserRoleById(userModel.Id);
                        //User user = new User();
                        //user.RoleId = role.RoleId;
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewBag.Message = "Yetkiniz bulunmamaktadır.";
                    }

                }

                else
                {
                    var email = "";
                    if (jsonUser.Borrower.Email == null)
                    {
                        jsonUser.Borrower.Email = "";

                    }
                    string boslukkontrol = (jsonUser.Borrower.Email.TrimStart()).TrimEnd();//boşluklarıalmak için
                    if (boslukkontrol != "")
                    {
                        email = jsonUser.Borrower.Email;
                    }
                    else
                    {

                        var sayi = jsonUser.Borrower.Cardnumber;

                        email = sayi + "admin@mail.com";
                    }
                    string branchCode = jsonUser.Borrower.Branchcode.ToString();
                    var branch = _BranchesService.GetBranchesList();
                    var x = branch.Where(p => p.branchcode == branchCode).FirstOrDefault();

                    User userModel = new User();
                    userModel.BranchCode = jsonUser.Borrower.Branchcode.ToString();
                    userModel.Name = jsonUser.Borrower.Firstname;
                    userModel.LastName = jsonUser.Borrower.Surname;
                    userModel.Email = email;
                    userModel.PhoneNumber = jsonUser.Borrower.Phonepro;
                    userModel.BranchCity = jsonUser.Borrower.City;
                    userModel.CardNumber = jsonUser.Borrower.Cardnumber;
                    userModel.BranchName = x.branchname;
                    userModel.RoleId = 2;
                    userModel.UserGroupId = 2;

                    UserCreate(userModel);
                    ResponseLoginViewModel.Email = userModel.Email;
                    ResponseLoginViewModel.Password = "E-kitap123456e";
                    LoginViewModel LoginModel = new LoginViewModel();
                    LoginModel.Email = userModel.Email;
                    LoginModel.Password = "E-kitap123456e";

                    var userModel2 = _userService.Login(LoginModel.Email, LoginModel.Password);
                    if (userModel2 != null)
                    {
                        ResponseLoginViewModel.Token = userModel2.Auth_token;
                        Session["login"] = userModel2;
                        Session["User"] = _userService.FindByUserId(userModel2.Id);
                        //var role = _userRoleService.FindUserRoleById(userModel.Id);
                        //User user = new User();
                        //user.RoleId = role.RoleId;
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewBag.Message = "Yetkiniz bulunmamaktadır.";
                    }



                }




                return RedirectToAction("KohaLogin", "Login");


            }
            else { 

                ViewBag.Title = "Kullanıcı Giriş Bilgileriniz Hatalı";
                return View(model);
            }
            return View(sonuc);
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }
        public ActionResult KohaLogin()
        {
            return View();
        }

 

        public static void UserCreate(User model)
        {

            var client = new RestClient("" + ApiUrlViewModel.Url + "User");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
            request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
            request.AddHeader("x-ip", RequestExtender.GetIp());
            request.AddHeader("x-browser", RequestExtender.GetBrowser());
            request.AddHeader("x-os", RequestExtender.GetUserPlatform());

            request.AlwaysMultipartFormData = true;
            request.AddParameter("Email", model.Email);
            request.AddParameter("Name", model.Name);
            request.AddParameter("LastName", model.LastName);
            request.AddParameter("PhoneNumber", model.PhoneNumber);
            request.AddParameter("UserGroupId", model.UserGroupId);
            request.AddParameter("BranchName", model.BranchName);
            request.AddParameter("BranchCode", model.BranchCode);
            request.AddParameter("BranchCity", model.BranchCity);
            request.AddParameter("RoleId", model.RoleId);
            request.AddParameter("CardNumber", model.CardNumber);
            request.AddParameter("KohaUser", 1);
            request.AddParameter("IsActive", true);
            IRestResponse response = client.Execute(request);

            JsonConvert.DeserializeObject(response.Content);
            
        }
        public static void UserEdit(User model)
        {
            var client = new RestClient("" + ApiUrlViewModel.Url + $"User?id={model.Id}");

            client.Timeout = -1;
            var request = new RestRequest(Method.PUT);
            request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
            request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
            request.AddHeader("x-ip", RequestExtender.GetIp());
            request.AddHeader("x-browser", RequestExtender.GetBrowser());
            request.AddHeader("x-os", RequestExtender.GetUserPlatform());

            request.AlwaysMultipartFormData = true;
            request.AddParameter("Email", model.Email);
            request.AddParameter("Name", model.Name);
            request.AddParameter("LastName", model.LastName);
            request.AddParameter("PhoneNumber", model.PhoneNumber);
            request.AddParameter("UserGroupId", model.UserGroupId);
            request.AddParameter("BranchName", model.BranchName);
            request.AddParameter("BranchCode", model.BranchCode);
            request.AddParameter("BranchCity", model.BranchCity);
            request.AddParameter("RoleId", model.RoleId);
            request.AddParameter("CardNumber", model.CardNumber);

            request.AddParameter("IsActive", true);
            IRestResponse response = client.Execute(request);

            JsonConvert.DeserializeObject(response.Content);






        }     

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            if (SecurityService.IsFloodRequest(UserActivityType.PasswordResetRequest, 15))
            {
                return RedirectToAction("TooManyRequest", "Error");
            }

            if (ModelState.IsValid)
            {
                var user = _userService.ForgotPassword(model.Email);
                if (user != null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

    }
}