﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.BranchesService;
using E_Kit.Service.Common;
using E_Kitap.Filters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class BranchRelationController : Controller
    {
         public ActionResult Index()
        {
              return View("Index");
        }

        [HttpPost]
        public ActionResult Data(string model)
            {
            if (model.ToUpper()== "AFYON")
            {
                model = "AFYONKARAH";
            }
            else if(model.ToUpper() == "AFYONKARAHİSAR")
            {
                model = "AFYONKARAH";
            }
            else if (model.ToUpper() == "KAHRAMANMARAŞ")
            {
                model = "KAHRAMANMA";
            }
            var client = new RestClient("http://koha-app.ekutuphane.gov.tr/api/branches/GetBranchRelationList/"+model.ToUpper());
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddParameter("application/json", "", ParameterType.RequestBody);
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            List<BranchRelation.BranchRelationData> jsonArrayAttribute = JsonConvert.DeserializeObject<List<BranchRelation.BranchRelationData>>(response.Content ?? "");

            ViewBag.BranchRelationList = jsonArrayAttribute;

            return Json(new { Data = response.Content });
        }




    }


     

    }