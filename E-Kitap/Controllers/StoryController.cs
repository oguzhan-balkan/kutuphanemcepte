﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.StoryService;
using E_Kit.Service.Common;
using E_Kitap.Filters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class StoryController : Controller
    {
        // GET: Story
        private readonly StoryService _StoryService = new StoryService();
        private readonly E_Kit.Service.BranchCategoriesService.BranchCategoriesService _branchCategoriesService = new E_Kit.Service.BranchCategoriesService.BranchCategoriesService();
        private readonly E_Kit.Service.BranchesService.BranchesService _BranchesService = new E_Kit.Service.BranchesService.BranchesService();
        public ActionResult Index()
        {
            
            return View(_StoryService.GetStoryList());
        }
        public ActionResult Create()
        {
            ViewBag.BranchCategoriesList = _branchCategoriesService.GetBranchCategoriesList();
            return View(new Story { IsActive = true });
        }
        [HttpPost]
        public ActionResult Create(Story model)
        {
           
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient(""+ ApiUrlViewModel.Url +"Story");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    using (var ms = new MemoryStream())
                    {
                        model.Image.InputStream.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        var Bytes = ms.Length;
                        if(Bytes>=1240000)
                        {
                            ViewBag.BranchCategoriesList = _branchCategoriesService.GetBranchCategoriesList();
                            ViewBag.Alert = "Dosya Boyutu Max 1Mb Olmalı";
                            return View(new Story { IsActive = true });

                            
                        }
                        request.AddFile("image", fileBytes, model.Image.FileName);
                    }
                    if(model.City== "undefined" && model.District== "undefined")
                    {
                        model.City = "ALL";
                        model.District = "ALL";
                    }
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("titleTr", model.TitleTR);
                    request.AddParameter("titleEn", model.TitleEN);
                    request.AddParameter("textTr", model.TextTR);
                    request.AddParameter("textEn", model.TextEN);
                    request.AddParameter("BranchCode", model.BranchCode);
                    request.AddParameter("City", model.City);
                    request.AddParameter("Url", model.Url);
                    request.AddParameter("UrlStatus", model.UrlStatus);
                    request.AddParameter("EndDate", model.EndDate.ToString("yyyy-MM-dd"));
                    request.AddParameter("District", model.District);
                    request.AddParameter("IsActive", true);
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            ViewBag.BranchCategoriesList = _branchCategoriesService.GetBranchCategoriesList();

            return View(_StoryService.FindStoryById(id));
        }

        [HttpPost]
        public ActionResult Edit(Story model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient(""+ApiUrlViewModel.Url+ $"Story?id={model.Id}");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.PUT);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    if (model.Image != null)
                    {
                        using (var ms = new MemoryStream())
                        {
                            model.Image.InputStream.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            request.AddFile("image", fileBytes, model.Image.FileName);
                        }
                    }
                   
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("titleTr", model.TitleTR);
                    request.AddParameter("titleEn", model.TitleEN);
                    request.AddParameter("textTr", model.TextTR);
                    request.AddParameter("textEn", model.TextEN);
                    request.AddParameter("IsActive", model.IsActive);
                    request.AddParameter("BranchCode", model.BranchCode);
                    request.AddParameter("City", model.City);
                    request.AddParameter("Url", model.Url);
                    request.AddParameter("EndDate", model.EndDate);
                    request.AddParameter("UrlStatus", model.UrlStatus);
                    request.AddParameter("District", model.District);
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }


        public ActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _StoryService.DeleteStory(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}