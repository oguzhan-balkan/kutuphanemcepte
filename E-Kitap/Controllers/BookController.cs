﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.ApiClient;
using E_Kit.Service.AuthorService;
using E_Kit.Service.BookService;
using E_Kit.Service.CategoryService;
using E_Kit.Service.Common;
using E_Kit.Service.LanguageService;
using E_Kit.Service.PublisherService;
using E_Kit.Service.TagService;
using E_Kitap.Filters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class BookController : Controller
    {
        private readonly BookService _bookService = new BookService();
        private readonly PublisherService _publisherService = new PublisherService();
        private readonly LanguageService _languageService = new LanguageService();
        private readonly AuthorService _authorService = new AuthorService();
        private readonly TagService _tagService = new TagService();
        private readonly CategoryService _categoryService = new CategoryService();
        public ActionResult Index()
        {
            var model = _bookService.GetBookList();
            return View(model);
        }
        public ActionResult Create()
        {
            ViewBag.Publisher = new SelectList(_publisherService.GetPublisherSelectList(), "Id", "Name");
            ViewBag.Language = new SelectList(_languageService.GetLanguageSelectList(), "Id", "Name");
            ViewBag.Tag = new SelectList(_tagService.GetTagSelectList(), "Id", "Name");
            ViewBag.Author = new SelectList(_authorService.GetAuthorSelectList(), "Id", "Name");
            ViewBag.Category = new SelectList(_categoryService.GetCategorySelectList(), "Id", "Name");
            return View(new Book { IsActive = true });
        }
        [HttpPost]
        public ActionResult Create(Book model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient("" + ApiUrlViewModel.Url + "Book");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    if (model.File != null)
                    {
                        using (var ms = new MemoryStream())
                        {
                            model.File.InputStream.CopyTo(ms);
                            var fileByte = ms.ToArray();
                            request.AddFile("File", fileByte, model.File.FileName);
                        }
                    }
                    if (model.Image != null)
                    {
                        using (var ms = new MemoryStream())
                        {
                            model.Image.InputStream.CopyTo(ms);
                            var imageByte = ms.ToArray();
                            request.AddFile("Image", imageByte, model.Image.FileName);
                        }
                    }
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("PublisherId", model.PublisherId);
                    request.AddParameter("LanguageId", model.LanguageId);
                    request.AddParameter("PageCount", model.PageCount);
                    request.AddParameter("Name", model.Name);
                    request.AddParameter("PublicationYear", model.PublicationYear);
                    request.AddParameter("IntroductionTr", model.IntroductionTr);
                    request.AddParameter("IntroductionEn", model.IntroductionEn);
                    request.AddParameter("ISBN", model.ISBN);
                    request.AddParameter("ExpirationDate", model.ExpirationDate);
                    request.AddParameter("IsOffline", model.IsOffline);
                    request.AddParameter("IsMultiUserRental", model.IsMultiUserRental);
                    request.AddParameter("IsExtensibleRental", model.IsExtensibleRental);
                    request.AddParameter("IsTermRental", model.IsTermRental);
                    request.AddParameter("RentalTermDay", model.RentalTermDay);
                    request.AddParameter("IsActive", model.IsActive);
                    for (int i = 0; i < model.Authors.Length; i++)
                    {
                        request.AddParameter($"authors[{i}]", model.Authors[i]);
                    }
                    for (int i = 0; i < model.Tags.Length; i++)
                    {
                        request.AddParameter($"tags[{i}]", model.Tags[i]);
                    }
                    for (int i = 0; i < model.Categories.Length; i++)
                    {
                        request.AddParameter($"categories[{i}]", model.Categories[i]);
                    }
                    IRestResponse response = client.Execute(request);
                    BltHttpResponseContent content = JsonConvert.DeserializeObject<BltHttpResponseContent>(response.Content);
                    if (!content.IsSuccess)
                    {
                        ViewBag.Publisher = new SelectList(_publisherService.GetPublisherSelectList(), "Id", "Name");
                        ViewBag.Language = new SelectList(_languageService.GetLanguageSelectList(), "Id", "Name");
                        ViewBag.Tag = new SelectList(_tagService.GetTagSelectList(), "Id", "Name");
                        ViewBag.Author = new SelectList(_authorService.GetAuthorSelectList(), "Id", "Name");
                        ViewBag.Category = new SelectList(_categoryService.GetCategorySelectList(), "Id", "Name");
                        ViewBag.Message = content.Message;
                        return View();
                    }
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            var data = _bookService.FindBookById(id);
            int[] tagSize = Array.ConvertAll(data.Tags, s => int.Parse(s));
            int[] authorSize = Array.ConvertAll(data.Authors, s => int.Parse(s));
            int[] categorySize = Array.ConvertAll(data.Categories, s => int.Parse(s));
            ViewBag.Publisher = new SelectList(_publisherService.GetPublisherSelectList(), "Id", "Name", data.PublisherId);
            ViewBag.Language = new SelectList(_languageService.GetLanguageSelectList(), "Id", "Name", data.LanguageId);
            ViewBag.Author = new MultiSelectList(_authorService.GetAuthorSelectList(), "Id", "Name", authorSize);
            ViewBag.Tag = new MultiSelectList(_tagService.GetTagSelectList(), "Id", "Name", tagSize);
            ViewBag.Category = new MultiSelectList(_categoryService.GetCategorySelectList(), "Id", "Name", categorySize);
            return View(data);
        }

        [HttpPost]
        public ActionResult Edit(Book model)
        {
            try
            {

                var client = new RestClient("" + ApiUrlViewModel.Url + $"Book");
                client.Timeout = -1;
                var request = new RestRequest(Method.PUT);
                request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                request.AddHeader("x-ip", RequestExtender.GetIp());
                request.AddHeader("x-browser", RequestExtender.GetBrowser());
                request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                if (model.File != null)
                {
                    using (var ms = new MemoryStream())
                    {
                        model.File.InputStream.CopyTo(ms);
                        var fileByte = ms.ToArray();
                        request.AddFile("File", fileByte, model.File.FileName);
                    }
                }
                if (model.Image != null)
                {
                    using (var ms = new MemoryStream())
                    {

                        model.Image.InputStream.CopyTo(ms);
                        var imageByte = ms.ToArray();
                        request.AddFile("Image", imageByte, model.Image.FileName);
                    }
                }
                request.AlwaysMultipartFormData = true;
                request.AddParameter("id", model.Id, ParameterType.QueryString);
                request.AddParameter("PublisherId", model.PublisherId);
                request.AddParameter("LanguageId", model.LanguageId);
                request.AddParameter("PageCount", model.PageCount);
                request.AddParameter("Name", model.Name);
                request.AddParameter("PublicationYear", model.PublicationYear);
                request.AddParameter("IntroductionTr", model.IntroductionTr);
                request.AddParameter("IntroductionEn", model.IntroductionEn);
                request.AddParameter("ISBN", model.ISBN);
                request.AddParameter("ExpirationDate", model.ExpirationDate);
                request.AddParameter("IsOffline", model.IsOffline);
                request.AddParameter("IsMultiUserRental", model.IsMultiUserRental);
                request.AddParameter("IsExtensibleRental", model.IsExtensibleRental);
                request.AddParameter("IsTermRental", model.IsTermRental);
                request.AddParameter("RentalTermDay", model.RentalTermDay);
                request.AddParameter("IsActive", model.IsActive);
                for (int i = 0; i < model.Authors.Length; i++)
                {
                    request.AddParameter($"authors[{i}]", model.Authors[i]);
                }
                for (int i = 0; i < model.Tags.Length; i++)
                {
                    request.AddParameter($"tags[{i}]", model.Tags[i]);
                }
                for (int i = 0; i < model.Categories.Length; i++)
                {
                    request.AddParameter($"categories[{i}]", model.Categories[i]);
                }
                IRestResponse response = client.Execute(request);
                JsonConvert.DeserializeObject(response.Content);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();

        }

        public ActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _bookService.DeleteBook(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}