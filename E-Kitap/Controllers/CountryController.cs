﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.Common;
using E_Kit.Service.CountryService;
using E_Kitap.Filters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class CountryController : Controller
    {
        private readonly CountryService _countryService = new CountryService();
        // GET: Country
        public ActionResult Index()
        {
            return View(_countryService.GetCountryList());
        }

        public ActionResult Create()
        {
            return View(new Country { IsActive = true });
        }
        [HttpPost]
        public ActionResult Create(Country model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient("" + ApiUrlViewModel.Url + "Country");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    using (var ms = new MemoryStream())
                    {
                        model.Image.InputStream.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        request.AddFile("Image", fileBytes, model.Image.FileName);
                    }

                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("Name", model.Name);
                    request.AddParameter("Code", model.Code);
                    request.AddParameter("PhoneCode", model.PhoneCode);
                    request.AddParameter("IsActive", true);
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            var model = _countryService.FindCountryById(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Country model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient("" + ApiUrlViewModel.Url + $"Country?id={model.Id}");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.PUT);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", RequestExtender.GetIp());
                    request.AddHeader("x-browser", RequestExtender.GetBrowser());
                    request.AddHeader("x-os", RequestExtender.GetUserPlatform());
                    if (model.Image != null)
                    {
                        using (var ms = new MemoryStream())
                        {
                            model.Image.InputStream.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            request.AddFile("Image", fileBytes, model.Image.FileName);
                        }
                    }
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("Name", model.Name);
                    request.AddParameter("Code", model.Code);
                    request.AddParameter("PhoneCode", model.PhoneCode);
                    request.AddParameter("IsActive", model.IsActive);
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _countryService.DeleteCountry(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}