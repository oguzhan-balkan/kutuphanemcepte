﻿using E_Kit.Data.Entities;
using E_Kit.Service.CategoryService;
using E_Kitap.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class CategoryController : Controller
    {
        // GET: Category
        private readonly CategoryService _categoryService = new CategoryService();
        public ActionResult Index()
        {
            return View(_categoryService.GetCategoryList());
        }
        public ActionResult Create()
        {
            return View(new Category { IsActive = true });
        }
        [HttpPost]
        public ActionResult Create(Category model, string favcolor)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.ColorCode = favcolor;
                    _categoryService.InsertCategory(model);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;

            }
            return View();
        }
        public ActionResult Edit(int id)
        {

            return View(_categoryService.FindCategoryById(id));
        }

        [HttpPost]
        public ActionResult Edit(Category model, string favcolor)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.ColorCode = favcolor;
                    _categoryService.EditCategory(model);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }


        public ActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _categoryService.DeleteCategory(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}