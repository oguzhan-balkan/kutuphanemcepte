﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
 using E_Kitap.Filters;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace E_Kitap.Controllers
{
    [Auth]
    public class ActivityCategoryController : Controller
    {
        // GET: ActivityCategory
        private readonly E_Kit.Service.ActivityCategoryService.ActivityCategoryService _ActivityCategoryService = new E_Kit.Service.ActivityCategoryService.ActivityCategoryService();
         public ActionResult Index()
        {
            
            return View(_ActivityCategoryService.GetActivityCategoryList());
        }
        public ActionResult Create()
        {
             return View();
        }
        [HttpPost]
        public ActionResult Create(ActivityCategory model)
        {
           
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient(""+ ApiUrlViewModel.Url +"ActivityCategory");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", E_Kit.Service.Common.RequestExtender.GetIp());
                    request.AddHeader("x-browser", E_Kit.Service.Common.RequestExtender.GetBrowser());
                    request.AddHeader("x-os", E_Kit.Service.Common.RequestExtender.GetUserPlatform());

                    model.CreateDate = DateTime.Now;
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("titleTr", model.TitleTR);
                    request.AddParameter("titleEn", model.TitleEN);
                    request.AddParameter("CreateDate", model.CreateDate.ToString("yyyy-MM-dd"));
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
 
            return View(_ActivityCategoryService.FindActivityCategoryById(id));
        }

        [HttpPost]
        public ActionResult Edit(ActivityCategory model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var client = new RestClient(""+ApiUrlViewModel.Url+ $"ActivityCategory?id={model.Id}");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.PUT);
                    request.AddHeader("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                    request.AddHeader("Authorization", "Bearer " + ResponseLoginViewModel.Token);
                    request.AddHeader("x-ip", E_Kit.Service.Common.RequestExtender.GetIp());
                    request.AddHeader("x-browser", E_Kit.Service.Common.RequestExtender.GetBrowser());
                    request.AddHeader("x-os", E_Kit.Service.Common.RequestExtender.GetUserPlatform());
                    
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("titleTr", model.TitleTR);
                    request.AddParameter("titleEn", model.TitleEN);
                    request.AddParameter("CreateDate", model.CreateDate.ToString("yyyy-MM-dd"));
                    IRestResponse response = client.Execute(request);
                    JsonConvert.DeserializeObject(response.Content);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
            }
            return View();
        }


        public ActionResult Delete(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _ActivityCategoryService.DeleteActivityCategory(id);
                }
            }
            catch (Exception e)
            {
                var ex = e.Message;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}