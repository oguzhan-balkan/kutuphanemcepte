// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup

importScripts('https://www.gstatic.com/firebasejs/8.2.10/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.10/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyBboMcg7F89molp8ybydEUXGFzI8pqwzHk",
    authDomain: "kohachat.firebaseapp.com",
    databaseURL: "https://kohachat-default-rtdb.firebaseio.com",
    projectId: "kohachat",
    storageBucket: "kohachat.appspot.com",
    messagingSenderId: "453890050350",
    appId: "1:453890050350:web:0fc375e8cf675242f2d2bf",
    measurementId: "G-WSM79GKJNP"
});

const messaging = firebase.messaging();


messaging.onBackgroundMessage(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
    var payloadJson = JSON.parse(payload.data.payload);
    const notificationTitle = payloadJson.UserName;
  const notificationOptions = {
      body: payloadJson.SendMessage,
        icon: '/favicon.ico'
  };

    var clientId = document.getElementById('userId').value;

    if (clientId === payloadJson.ClientId) {
        var userId = document.getElementById("UserId").value;
        var position = payloadJson.UserId == userId ? 'left' : 'right';
        var _message = messageBox(payloadJson.UserName, payloadJson.SendMessage, '1', position);
        document.getElementById('namesList').append(_message);
    } else {
        

        var currentCount = Number(document.querySelector('[data-notify="' + payloadJson.UserId + '"]').innerHTML);

        var count = currentCount + 1;
        document.querySelector('[data-notify="' + payloadJson.UserId + '"]').innerHTML = count;
        document.querySelector('[data-notify="' + payloadJson.UserId + '"]').style.display = 'block';
    }

  self.registration.showNotification(notificationTitle,
    notificationOptions);
});
