﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace E_Kit.Data.Entities
{
    public class Catalogue
    {
        public string Title { get; set; }
        public string PublisherCode { get; set; }
        public string Author { get; set; }    
        public string ISBN { get; set; }

        public CatalogueSearch CatalogueSearch { get; set; }

    }

    public class CatalogueSearch
    {
        public string searchType;
        public string searchKey;
        public int homebranch;
        public int pagesize;
        public int pagenumber;

    }
}
