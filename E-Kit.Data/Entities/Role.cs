﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace E_Kit.Data.Entities
{
    public class Role : BaseModel
    {
        [Required,StringLength(128)]
        public string Name { get; set; }
       
        [Required]
        public bool IsActive { get; set; }
       
    }
}
