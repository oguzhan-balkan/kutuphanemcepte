﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Data.Entities.Enums
{
    public enum UserActivityType
    {
        UserLogin = 0,
        FailedLogin = 149,

        PanelLogin = 150,
        PanelRegister = 151,
        PanelConfirmEmail = 152,
        PanelForgetPassword = 153,
        PanelReset = 154,
        PanelSendCode = 154,
        PanelExternalLogin = 155,
        InvalidPanelLogin = 150,


        Cikis = 1,
        UpdateProfile = 2,

        AdresEkleme = 3,
        UyelikKapatma = 4,
        IletisimFormuDoldurma = 5,
        UserRegister = 6,

        JoinTheNewsletter = 7,
        PasswordResetRequest = 8,
        PasswordResetRequestConfirm = 9,

        AddToFavourites = 10,
        ActivateMembership = 11,
        MergeMembership = 12,
        MyTickets = 13,

        ChangePasswordRequest = 14,
        ChangeEmail = 15,
        ConfirmEmail = 16,



        FullSearch = 20,
        AutoCompleteSearch = 20,
        AddToBasket = 31,



        PaymentProcess = 32,
        PaymentStep = 33,
        UsingCoupon = 34,
        DilDegistirme = 40,
        ChangeTheCurrency = 41,
        ContentPageView = 42,
        AddComment = 43,
        LikeContent = 44,
        ShareWithEmail = 45,

        CancelTicket = 46,

        ExternalLogin = 47,
        AccountSendCode = 48,
        DeleteFromBasket = 49,
        KoltukSecimi = 50,
        SelectSeat = 51,

        FailedUserLogin = 52,

        OtherDangrouesRequest = 54,


        InvalidPaymentProcessRequest = 55,
        InvalidTokenFormatRequest = 56,
        InvalidEmailPasswordReset = 57,

        InvalidRegisterFormat = 58,
        InvalidProfileUpdateFormat = 59,
        InvalidEmailLogin = 60,
        InvalidEmailLoginApi = 61




    }
}
