﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Data.Entities
{
    public class UserGroup : BaseModel
    {
        public string Name { get; set; }
    }
}
