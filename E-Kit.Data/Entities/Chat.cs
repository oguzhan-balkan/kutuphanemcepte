﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace E_Kit.Data.Entities
{
    public class Chat : BaseModel
    {
         public string UserName { get; set; }
         public string SendMessage { get; set; }
         public string BranchCode { get; set; }
         public string BranchName { get; set; }
         public string ClientId { get; set; }
         public string UserId { get; set; }
         public string ClientName { get; set; }
      }
}
