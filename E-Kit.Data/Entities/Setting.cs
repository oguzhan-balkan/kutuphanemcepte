﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Data.Entities
{
    public class Setting : BaseModel
    {
        public string EmailAddress { get; set; }
        public string EmailPassword { get; set; }
        public string EmailHost { get; set; }
        public int EmailPort { get; set; }
        public bool EmailSsl { get; set; }
        public string SocialFacebookUrl { get; set; }
        public string SocialInstagramUrl { get; set; }
        public string SocialLinkedinUrl { get; set; }
        public string SocialTwitterUrl { get; set; }
        public string SocialYoutubeUrl { get; set; }
        public int DefaultRentalTermDay { get; set; }
        public int IosMaxVersion { get; set; }
        public int AndroidMaxVersion { get; set; }
        public string BaseWebSiteUrl { get; set; }
    }
}
