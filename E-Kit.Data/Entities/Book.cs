﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace E_Kit.Data.Entities
{
    public class Book : BaseModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int PublisherId { get; set; }
        public virtual Publisher Publisher { get; set; }
        public string PublisherName { get; set; }
        [Required]
        public int LanguageId { get; set; }
        public virtual Language Language { get; set; }
        public string LanguageName { get; set; }
        [Required]
        public int PageCount { get; set; }
        [Required]
        public string[] Authors { get; set; }
        [Required]
        public string[] Tags { get; set; }
        [Required]
        public string[] Categories { get; set; }
        public HttpPostedFileBase File { get; set; }
        public string FileUrl { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string ImageUrl { get; set; }
        [Required]
        public int PublicationYear { get; set; }
        [Required]
        public string IntroductionTr { get; set; }
        public string IntroductionEn { get; set; }
        public string ISBN { get; set; }
        public DateTime? ExpirationDate { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public bool IsOffline { get; set; }
        [Required]
        public bool IsMultiUserRental { get; set; }
        [Required]
        public bool IsExtensibleRental { get; set; }
        [Required]
        public bool IsTermRental { get; set; }

        public int? RentalTermDay { get; set; }
    }
}
