﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace E_Kit.Data.Entities
{
    public class BranchCategories
    {
        [Display(Name ="")]
        public string categorycode { get; set; }

        [Display(Name = "")]
        public string categoryname { get; set; }

        [Display(Name = "")]
        public string codedescription { get; set; }
  
     }
}
