﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace E_Kit.Data.Entities
{
    public class Branches
    {
        [Display(Name ="")]
        public string branchcode { get; set; }

        [Display(Name = "")]
        public string branchname { get; set; }

        [Display(Name = "")]
        public string branchaddress1 { get; set; }

        [Display(Name = "")]
        public string branchaddress2 { get; set; }

        [Display(Name = "")]
        public string branchaddress3 { get; set; }

        [Display(Name = "")]
        public string branchzip { get; set; }

        [Display(Name = "")]
        public string branchcity { get; set; }

        [Display(Name = "")]
        public string branchstate { get; set; }

        [Display(Name = "")]
        public string branchcountry { get; set; }

        [Display(Name = "")]
        public string branchphone { get; set; }

        [Display(Name = "")]
        public string branchfax { get; set; }

        [Display(Name = "")]
        public string branchemail { get; set; }

        [Display(Name = "")]
        public string branchreplyto { get; set; }

        [Display(Name = "")]
        public string branchreturnpath { get; set; }

        [Display(Name = "")]
        public string branchurl { get; set; }

        [Display(Name = "")]
        public string issuing { get; set; }

        [Display(Name = "")]
        public string branchip { get; set; }

        [Display(Name = "")]
        public string branchprinter { get; set; }

        [Display(Name = "")]
        public string branchnotes { get; set; }

        [Display(Name = "")]
        public string opac_info { get; set; }

        [Display(Name = "")]
        public string branchdayfrom { get; set; }

        [Display(Name = "")]
        public string branchdayto { get; set; }

        [Display(Name = "")]
        public string branchtimeh { get; set; }

        [Display(Name = "")]
        public string branchtimehs { get; set; }

        [Display(Name = "")]
        public string branchopen { get; set; }

        [Display(Name = "")]
        public string latitude { get; set; }

        [Display(Name = "")]
        public string longitude { get; set; }

        [Display(Name = "")]
        public string ebook_status { get; set; }

        [Display(Name = "")]
        public string branchrelations { get; set; }

     }
}
