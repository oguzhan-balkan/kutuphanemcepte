﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace E_Kit.Data.Entities
{
    public class ActivityCategory : BaseModel
    {
        [Required,StringLength(256)]
        public string TitleTR { get; set; }
        [StringLength(256)]
        public string TitleEN { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreatedBy { get; set; }
        
 
    }
}
