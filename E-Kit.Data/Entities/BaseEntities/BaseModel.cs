﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Web.Mvc;

namespace E_Kit.Data.Entities.BaseEntities
{
    public class BaseModel
    {

        [Key]
        [HiddenInput]
        public int Id { get; set; }
    }
}
