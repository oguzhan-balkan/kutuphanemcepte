﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace E_Kit.Data.Entities
{
    public class AboutApplication
    {
        [Required]
        [AllowHtml]
        public string TextTr { get; set; }
        [AllowHtml]
        public string TextEn { get; set; }
    }

    public class PrivacyPolicy
    {
        [Required]
        [AllowHtml]
        public string TextTr { get; set; }
        [AllowHtml]
        public string TextEn { get; set; }
    }

    public class Contact
    {
        [Required]
        [StringLength(64)]
        public string Tel { get; set; }
        [Required]
        [StringLength(64)]
        public string Email { get; set; }
        [Required]
        [StringLength(255)]
        public string Address { get; set; }
        [Required]
        [StringLength(64)]
        public string Location { get; set; }
    }
}
