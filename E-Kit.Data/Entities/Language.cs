﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Data.Entities
{
    public class Language : BaseModel
    {
        [Required]
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
