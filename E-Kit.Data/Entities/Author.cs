﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace E_Kit.Data.Entities
{
    public class Author : BaseModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "LastName")]
        public string LastName { get; set; }
        public string Title { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string ImageUrl { get; set; }
        [Required]
        public string AboutTr { get; set; }
        public string AboutEn { get; set; }
        public bool IsActive { get; set; }
    }
}
