﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace E_Kit.Data.Entities
{
    public class Activity : BaseModel
    {
        [Required,StringLength(256)]
        public string TitleTR { get; set; }
        [StringLength(256)]
        public string TitleEN { get; set; }
        [AllowHtml]
        [Required, MaxLength]
        public string TextTR { get; set; }
        [AllowHtml]
        [MaxLength]
        public string TextEN { get; set; }
        public string ImageUrl { get; set; }
        public HttpPostedFileBase Image { get; set; }
        [Required]
        public bool IsActive { get; set; }
        public string BranchCode { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public bool UrlStatus { get; set; }
        public string Url { get; set; }
        public string ImageFile { get; set; }
        public int Number { get; set; }
        public bool Price { get; set; }
        public int ActivityCategoryId { get; set; }
        public string ActivityCategoryTitle { get; set; }
 
    }
}
