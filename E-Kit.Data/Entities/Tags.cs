﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Data.Entities
{
    public class Tags : BaseModel
    {
        public string NameTr { get; set; }
        public string NameEn { get; set; }
        public string ColorCode { get; set; }
        public bool IsActive { get; set; }
    }
}
