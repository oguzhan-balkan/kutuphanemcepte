﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.Globalization;
  
namespace E_Kit.Data.Entities
{
    public partial class KohaUser
    {
        public Borrower Borrower { get; set; }
    }

    public partial class Borrower
    {
        public long Borrowernumber { get; set; }
        public string Cardnumber { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Title { get; set; }
        public object Othernames { get; set; }
        public object Initials { get; set; }
        public string Streetnumber { get; set; }
        public object Streettype { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Emailpro { get; set; }
        public string Phonepro { get; set; }
        public string BStreetnumber { get; set; }
        public object BStreettype { get; set; }
        public string BAddress { get; set; }
        public string BAddress2 { get; set; }
        public string BCity { get; set; }
        public object BState { get; set; }
        public string BZipcode { get; set; }
        public string BCountry { get; set; }
        public object BEmail { get; set; }
        public object BPhone { get; set; }
        public DateTimeOffset Dateofbirth { get; set; }
        public long Branchcode { get; set; }
        public string Categorycode { get; set; }
        public DateTimeOffset Dateenrolled { get; set; }
        public DateTimeOffset Dateexpiry { get; set; }
        public bool Gonenoaddress { get; set; }
        public bool Lost { get; set; }
        public object Debarred { get; set; }
        public object Debarredcomment { get; set; }
        public object Contactname { get; set; }
        public object Contactfirstname { get; set; }
        public object Contacttitle { get; set; }
        public long Guarantorid { get; set; }
        public string Borrowernotes { get; set; }
        public object Relationship { get; set; }
        public string Ethnicity { get; set; }
        public string Ethnotes { get; set; }
        public string Sex { get; set; }
        public object Password { get; set; }
        public long Flags { get; set; }
        public string Userid { get; set; }
        public string Opacnote { get; set; }
        public string Contactnote { get; set; }
        public string Sort1 { get; set; }
        public string Sort2 { get; set; }
        public string Altcontactfirstname { get; set; }
        public string Altcontactsurname { get; set; }
        public string Altcontactaddress1 { get; set; }
        public string Altcontactaddress2 { get; set; }
        public string Altcontactaddress3 { get; set; }
        public object Altcontactstate { get; set; }
        public string Altcontactzipcode { get; set; }
        public object Altcontactcountry { get; set; }
        public string Altcontactphone { get; set; }
        public object Smsalertnumber { get; set; }
        public long Privacy { get; set; }
        public string Engelli { get; set; }
        public bool Tur { get; set; }
        public long KurumId { get; set; }
        public string Yas { get; set; }
        public bool Mektup { get; set; }
        public object PlayerIds { get; set; }
        public object Referenceno { get; set; }
        public object OldIssue { get; set; }
    }








    //public partial class KohaUser
    //{
    //    //public KohaUser()
    //    //{
    //    //    borrower = new List<KohaUserData>();
    //    //}
    //    //public List<KohaUserData> borrower { get; set; }

    //    //public class KohaUserData
    //    //{
    //    //    public List<KohaUserData> borrower { get; set; }

    //    //    public string borrowernumber { get; set; }
    //    //    public string cardnumber { get; set; }
    //    //    public string surname { get; set; }
    //    //    public string firstname { get; set; }
    //    //    public string city { get; set; }
    //    //    public string email { get; set; }
    //    //    public string mobile { get; set; }
    //    //    public string phonepro { get; set; }
    //    //    public string dateofbirth { get; set; }
    //    //    public string userid { get; set; }
    //    //    public string branchcode { get; set; }

    //    //}


    //}


}
