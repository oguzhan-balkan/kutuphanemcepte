﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace E_Kit.Data.Entities
{
    public class Library : BaseModel
    {
         
        public string ImageUrl { get; set; }
        public HttpPostedFileBase Image { get; set; }
        [Required]
        public bool IsActive { get; set; }
        public string BranchCode { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
        public string Twitter { get; set; }
    }
}
