﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace E_Kit.Data.Entities
{
    public class Country : BaseModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string PhoneCode { get; set; }
        public string ImageUrl { get; set; }
        public HttpPostedFileBase Image { get; set; }
        [Required]
        public bool IsActive { get; set; }
    }
}
