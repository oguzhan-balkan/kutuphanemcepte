﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace E_Kit.Data.Entities
{
    public class User
    {
        public string Id { get; set; }
        public int UserGroupId { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public string UserGroupName { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsActive { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string BranchCity { get; set; }
        public string CardNumber { get; set; }
        public int RoleId { get; set; }
        public virtual UserRole UserRole { get; set; }


    }
}
