﻿using System.Collections.Generic;

namespace E_Kit.Data.Entities
{
    public partial class KohaTokenModel
    {
        public KohaTokenModel()
        {
            Token = new List<KohaTokenData>();
        }
        public List<KohaTokenData> Token { get; set; }

        public class KohaTokenData
        {
            public string onlogin { get; set; }

            public string token { get; set; }
            public string message { get; set; }
 
        }


    }
}
