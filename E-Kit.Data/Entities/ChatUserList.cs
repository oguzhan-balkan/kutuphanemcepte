﻿using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace E_Kit.Data.Entities
{
    public class ChatUserList : BaseModel
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string BranchCode { get; set; }
        [Required]
        public string BranchName { get; set; }

        [Required]
        public bool IsActive { get; set; }
    }
}
