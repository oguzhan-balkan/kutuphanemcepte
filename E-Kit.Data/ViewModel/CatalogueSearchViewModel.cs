﻿using E_Kit.Data.Entities;
using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Data.ViewModel
{
    public class CatalogueSearchViewModel
    {
        public string searchType { get; set; }
        public string searchKey { get; set; }
        public int homebranch { get; set; }
        public int pagesize { get; set; }
        public int pagenumber { get; set; }
        
    }
}
