﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Data.ViewModel
{
    public class ChangePasswordViewModel
    {
        public string Id { get; set; }
        [Required]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "Şifreniz eşleşmiyor. Tekrar deneyiniz")]
        public string ConfirmPassword { get; set; }
    }
}
