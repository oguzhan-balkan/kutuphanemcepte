﻿using System.ComponentModel.DataAnnotations;

namespace E_Kit.Data.ViewModel
{
    public class KohaLoginViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Tc No")]
        public string cardnumber { get; set; }
        [Required]
        [Display(Name = "Şifre")]
        public string password { get; set; }

    }

}