﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Data.ViewModel
{
    public class DropdownViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
