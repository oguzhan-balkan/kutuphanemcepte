﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Data.ViewModel
{
    public static class ResponseLoginViewModel
    {
        public static string Email { get; set; }
        public static string Password { get; set; }
        public static string Token { get; set; }
    }
}
