﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Data.ViewModel
{
    public static class ApiUrlViewModel
    {
        public static string PanelUrl { get { return "https://kutuphanemceptepanel.ktb.gov.tr/"; } }
        //   public static string PanelUrl { get { return "https://localhost:44326/"; } }
        //public static string Url { get { return "https://localhost:5001/"; } }

        public static string Url { get { return "https://kutuphanemcepteapi.ktb.gov.tr/"; } }
        //public static string Url { get { return "http://api.kentvakfi.dijitalsahne.com/"; } }

        public static string OtherUrl { get { return "http://koha-app.ekutuphane.gov.tr/"; } }

    }
}
