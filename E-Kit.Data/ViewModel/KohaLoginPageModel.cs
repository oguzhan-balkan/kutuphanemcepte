﻿using System.ComponentModel.DataAnnotations;

namespace E_Kit.Data.ViewModel
{
    public class KohaLoginPageModel
    {
        public string Result { get; set; }
        public KohaLoginViewModel KohaLoginViewModel { get; set; }

    }

}