﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace E_Kit.Data.ViewModel
{
    public class UserViewModel : IdentityUser
    {
        public string Id { get; set; }
        public int UserGroupId { get; set; }
        public string UserGroupName { get; set; }
        public string Auth_token { get; set; }
        public int Expires_in { get; set; }


    }
}
