﻿using E_Kit.Data.Entities;
using E_Kit.Data.Entities.BaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Data.ViewModel
{
    public class EditorBookViewModel : BaseModel
    {
        public int BookId { get; set; }
        public virtual Book Book { get; set; }
        public int SortNo { get; set; }
        public string BookName { get; set; }
        public string BookISBN { get; set; }
    }
}
