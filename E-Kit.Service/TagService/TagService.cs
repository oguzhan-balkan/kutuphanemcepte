﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.TagService
{
    public class TagService : ITagService
    {
        public bool DeleteTag(int id)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Tag?id={id}");
        }

        public bool EditTag(Tags model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Tag?id={model.Id}", new { NameTr = model.NameTr, NameEn = model.NameEn, colorCode = model.ColorCode, isActive = model.IsActive });
        }

        public Tags FindTagById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Tags>($"/Tag/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Tags> GetTagList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Tags>("/Tag");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<DropdownViewModel> GetTagSelectList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<DropdownViewModel>("/Dropdown/Tag");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertTag(Tags model)
        {
            return new BltHttpClient().PostReturnSingle<bool>("Tag", new { NameTr = model.NameTr, NameEn = model.NameEn, colorCode = model.ColorCode, isActive = model.IsActive });
        }
    }
}
