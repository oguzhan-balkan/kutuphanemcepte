﻿using E_Kit.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.TagService
{
    public interface ITagService
    {
        List<Tags> GetTagList();
        Tags FindTagById(int id);
        bool InsertTag(Tags model);
        bool EditTag(Tags model);
        bool DeleteTag(int id);
    }
}
