﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.CountryService
{
    public interface ICountryService
    {
        List<Country> GetCountryList();
        Country FindCountryById(int id);
        bool InsertCountry(Country model);
        bool EditCountry(Country model);
        bool DeleteCountry(int id);
        List<DropdownViewModel> GetCountrySelectList();
    }
}
