﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.CountryService
{
    public class CountryService : ICountryService
    {
        public bool DeleteCountry(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/Country?id={id}");
        }

        public bool EditCountry(Country model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Country?id={model.Id}", new { name = model.Name, code = model.Code , PhoneCode = model.PhoneCode
                ,Image = model.Image, isActive = model.IsActive });
        }

        public Country FindCountryById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Country>($"/Country/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Country> GetCountryList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Country>("/Country");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<DropdownViewModel> GetCountrySelectList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<DropdownViewModel>("/Country/SelectList");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertCountry(Country model)
        {
            return new BltHttpClient().PostReturnSingle<bool>("Country", new
            {
                name = model.Name,
                code = model.Code,
                PhoneCode = model.PhoneCode,
                Image = model.Image,
                isActive = model.IsActive
            });
        }
    }
}
