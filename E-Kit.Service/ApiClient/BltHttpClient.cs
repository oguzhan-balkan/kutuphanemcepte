﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.Common;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace E_Kit.Service.ApiClient
{
    public class BltHttpClient
    {
        public string Token = ResponseLoginViewModel.Token;
        public DateTime TokenExpireUtc = DateTime.UtcNow.AddDays(-1);

        public BltApiUser curUser = null;

        public string apiUsername = ResponseLoginViewModel.Email;
        public string apiPassword = ResponseLoginViewModel.Password;
        //public string apiBasePath = "http://api.kentvakfi.dijitalsahne.com";
        //public static string apiBasePath = "https://localhost:5001";
        public static string apiBasePath = "https://kutuphanemcepteapi.ktb.gov.tr";
        public static string OtherApiBasePath = "http://koha-app.ekutuphane.gov.tr/api";

        public string tokenPath = "/Auth/Login";

        public int tryLimit = 2;

        public BltHttpClient()
        {
            // using System.Net;
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            // Use SecurityProtocolType.Ssl3 if needed for compatibility reasons
        }

        public T GetSingle<T>(string path)
        {
            var item = (GetAsOperationResult<T>(new BltHttpRequest { RequestUrl = path }));
            return item.Result;
            //return Get<T>(new BltHttpRequest { RequestUrl = path });
        }

        public T Get<T>(string path)
        {
            return Get<T>(new BltHttpRequest { RequestUrl = path });
        }

        public T Get<T>(BltHttpRequest request)
        {
            request.RequestMethod = BltHttpMethod.GET;

            var httpResult = Do(request);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                return Activator.CreateInstance<T>();
            }

            try
            {
                var result = JsonConvert.DeserializeObject<T>(httpResult.ContentBody ?? "");
                return result;
            }
            catch (Exception)
            {
                return Activator.CreateInstance<T>();
            }
        }

        public OperationResult<T> GetAsOperationResult<T>(string path)
        {
            return GetAsOperationResult<T>(new BltHttpRequest { RequestUrl = path, RequestMethod = BltHttpMethod.GET });
        }

        public OperationResult<T> GetAsOperationResult<T>(BltHttpRequest request)
        {
            request.RequestMethod = BltHttpMethod.GET;
            var httpResult = Do(request);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                return Activator.CreateInstance<OperationResult<T>>();
            }

            try
            {
                var result = JsonConvert.DeserializeObject<OperationResult<T>>(httpResult.ContentBody ?? "");
                return result;
            }
            catch (Exception ex)
            {
                return Activator.CreateInstance<OperationResult<T>>();
            }

        }

        public OperationListResult<T> GetAsListResult<T>(string path)
        {
            return GetAsListResult<T>(new BltHttpRequest { RequestUrl = path });
        }

        public List<T> GetAsListResultOther<T>(string path)
        {
            return GetAsListResultOther<T>(new BltHttpRequest { RequestUrl = path });// other için kullanılacak
        }

        public OperationListResult<T> GetAsListResult<T>(string path, object values)
        {
            return GetAsListResult<T>(new BltHttpRequest { RequestUrl = path , RequestBody = values });
        }

        public List<T> GetAsListOny<T>(string path)
        {
            return GetAsListResult<T>(new BltHttpRequest { RequestUrl = path }).Result;
        }

        public OperationListResult<T> GetAsListResult<T>(BltHttpRequest request)
        {
            request.RequestMethod = BltHttpMethod.GET;
            var httpResult = Do(request);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                var result = Activator.CreateInstance<OperationListResult<T>>();
                result.SetError(httpResult.StatusDescription);
                return result;
            }

            try
            {
                var result = JsonConvert.DeserializeObject<OperationListResult<T>>(httpResult.ContentBody ?? "");
                return result;
            }
            catch (Exception ex)
            {
                var result = Activator.CreateInstance<OperationListResult<T>>();
                result.SetError(httpResult.StatusDescription);
                return result;

            }

        }


        public List<T> GetAsListResultOther<T>(BltHttpRequest request)
        {
            request.RequestMethod = BltHttpMethod.GET;// other işlemleri için kullanılacak 
            var httpResult = DoOther(request);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                var result = Activator.CreateInstance<List<T>>();
               // result.SetError(httpResult.StatusDescription);
                return result;
            }

            try
            {
                List<T> jsonArrayAttribute;
                jsonArrayAttribute= JsonConvert.DeserializeObject<List<T>>(httpResult.ContentBody ?? "");
                //var result = JsonConvert.DeserializeObject<OperationListResult<T>>(httpResult.ContentBody ?? "");
                 
                return jsonArrayAttribute;
            }
            catch (Exception ex)
            {
                var result = Activator.CreateInstance<List<T>>();
               // result.SetError(httpResult.StatusDescription);
                return result;

            }

        }

        public T Post<T>(string path, bool token = true)
        {
            return Post<T>(new BltHttpRequest { RequestUrl = path, RequestBody = null }, token);
        }

        public T Post<T>(string path, object values, bool token = true)
        {
            return Post<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token);
        }

        public void Post(string path, object values)
        {
            Post(new BltHttpRequest { RequestUrl = path, RequestBody = values });
        }

        public OperationListResult<T> PostReturnAsListResult<T>(string path, object values, bool token = true)
        {
            return PostReturnAsListResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token);
        }


        public List<T> PostReturnAsListOnly<T>(string path, bool token = true)
        {
            return PostReturnAsListResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = null }, token).Result;
        }

        public List<T> PostReturnAsListOnly<T>(string path, object values, bool token = true)
        {
            return PostReturnAsListResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token).Result;
        }

        public List<T> PostReturnAsListOnly<T>(string path)
        {
            return PostReturnAsListResult<T>(new BltHttpRequest { RequestUrl = path }).Result;
        }

        public T PostReturnSingle<T>(string path)
        {
            return PostReturnAsOperationResult<T>(new BltHttpRequest { RequestUrl = path }).Result;
        }

        public T PostReturnSingle<T>(string path, object values, bool token = true)
        {
            return PostReturnAsOperationResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token).Result;
        }


        public OperationListResult<T> PostReturnAsListResult<T>(BltHttpRequest request, bool token = true)
        {
            request.RequestMethod = BltHttpMethod.POST;
            var httpResult = Do(request, token: token);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                var result = Activator.CreateInstance<OperationListResult<T>>();
                result.SetError(httpResult.StatusDescription);
                return result;
            }

            try
            {
                var result = JsonConvert.DeserializeObject<OperationListResult<T>>(httpResult.ContentBody ?? "");
                return result;
            }
            catch (Exception ex)
            {
                var result = Activator.CreateInstance<OperationListResult<T>>();
                result.SetError(ex.Message);
                return result;
            }

        }

        public OperationResult<T> PostReturnAsOperationResult<T>(string path, object values, bool token = true)
        {
            return PostReturnAsOperationResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token: token);
        }

        public T Post<T>(BltHttpRequest request, bool token = true)
        {
            request.RequestMethod = BltHttpMethod.POST;
            var httpResult = Do(request, token: token);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                return Activator.CreateInstance<T>();
            }

            try
            {
                var result = JsonConvert.DeserializeObject<T>(httpResult.ContentBody ?? "");
                return result;
            }
            catch (Exception)
            {
                return Activator.CreateInstance<T>();
            }

        }

        public void Post(BltHttpRequest request, bool token = true)
        {
            request.RequestMethod = BltHttpMethod.POST;
            var httpResult = Do(request, token: token);
            return;
        }

        public OperationResult<T> PostReturnAsOperationResult<T>(BltHttpRequest request, bool token = true)
        {
            request.RequestMethod = BltHttpMethod.POST;
            var httpResult = Do(request, token: token);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                var result = Activator.CreateInstance<OperationResult<T>>();
                result.SetError(httpResult.StatusDescription);
                return result;
            }

            try
            {
                var result = JsonConvert.DeserializeObject<OperationResult<T>>(httpResult.ContentBody ?? "");
                return result;
            }
            catch (Exception ex)
            {
                var result = Activator.CreateInstance<OperationResult<T>>();
                result.SetError(ex.Message);
                return result;
            }
        }

        public T Put<T>(string path, bool token = true)
        {
            return Put<T>(new BltHttpRequest { RequestUrl = path, RequestBody = null }, token);
        }

        public T Put<T>(string path, object values, bool token = true)
        {
            return Put<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token);
        }

        public void Put(string path, object values)
        {
            Put(new BltHttpRequest { RequestUrl = path, RequestBody = values });
        }

        public OperationListResult<T> PutReturnAsListResult<T>(string path, object values, bool token = true)
        {
            return PutReturnAsListResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token);
        }


        public List<T> PutReturnAsListOnly<T>(string path, bool token = true)
        {
            return PutReturnAsListResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = null }, token).Result;
        }

        public List<T> PutReturnAsListOnly<T>(string path, object values, bool token = true)
        {
            return PostReturnAsListResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token).Result;
        }

        public List<T> PutReturnAsListOnly<T>(string path)
        {
            return PutReturnAsListResult<T>(new BltHttpRequest { RequestUrl = path }).Result;
        }

        public T PutReturnSingle<T>(string path)
        {
            return PostReturnAsOperationResult<T>(new BltHttpRequest { RequestUrl = path }).Result;
        }

        public T PutReturnSingle<T>(string path, object values, bool token = true)
        {
            return PutReturnAsOperationResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token).Result;
        }


        public OperationListResult<T> PutReturnAsListResult<T>(BltHttpRequest request, bool token = true)
        {
            request.RequestMethod = BltHttpMethod.PUT;
            var httpResult = Do(request, token: token);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                var result = Activator.CreateInstance<OperationListResult<T>>();
                result.SetError(httpResult.StatusDescription);
                return result;
            }

            try
            {
                var result = JsonConvert.DeserializeObject<OperationListResult<T>>(httpResult.ContentBody ?? "");
                return result;
            }
            catch (Exception ex)
            {
                var result = Activator.CreateInstance<OperationListResult<T>>();
                result.SetError(ex.Message);
                return result;
            }

        }

        public OperationResult<T> PutReturnAsOperationResult<T>(string path, object values, bool token = true)
        {
            return PutReturnAsOperationResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token: token);
        }

        public T Put<T>(BltHttpRequest request, bool token = true)
        {
            request.RequestMethod = BltHttpMethod.PUT;
            var httpResult = Do(request, token: token);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                return Activator.CreateInstance<T>();
            }

            try
            {
                var result = JsonConvert.DeserializeObject<T>(httpResult.ContentBody ?? "");
                return result;
            }
            catch (Exception)
            {
                return Activator.CreateInstance<T>();
            }

        }

        public void Put(BltHttpRequest request, bool token = true)
        {
            request.RequestMethod = BltHttpMethod.PUT;
            var httpResult = Do(request, token: token);
            return;
        }

        public OperationResult<T> PutReturnAsOperationResult<T>(BltHttpRequest request, bool token = true)
        {
            request.RequestMethod = BltHttpMethod.PUT;
            var httpResult = Do(request, token: token);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                var result = Activator.CreateInstance<OperationResult<T>>();
                result.SetError(httpResult.StatusDescription);
                return result;
            }

            try
            {
                var result = JsonConvert.DeserializeObject<OperationResult<T>>(httpResult.ContentBody ?? "");
                return result;
            }
            catch (Exception ex)
            {
                var result = Activator.CreateInstance<OperationResult<T>>();
                result.SetError(ex.Message);
                return result;
            }
        }

        public T Delete<T>(string path, bool token = true)
        {
            return Delete<T>(new BltHttpRequest { RequestUrl = path, RequestBody = null }, token);
        }

        public T Delete<T>(string path, object values, bool token = true)
        {
            return Delete<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token);
        }

        public void Delete(string path, object values)
        {
            Delete(new BltHttpRequest { RequestUrl = path, RequestBody = values });
        }

        public OperationListResult<T> DeleteReturnAsListResult<T>(string path, object values, bool token = true)
        {
            return DeleteReturnAsListResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token);
        }


        public List<T> DeleteReturnAsListOnly<T>(string path, bool token = true)
        {
            return DeleteReturnAsListResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = null }, token).Result;
        }

        public List<T> DeleteReturnAsListOnly<T>(string path, object values, bool token = true)
        {
            return DeleteReturnAsListResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token).Result;
        }

        public List<T> DeleteReturnAsListOnly<T>(string path)
        {
            return DeleteReturnAsListResult<T>(new BltHttpRequest { RequestUrl = path }).Result;
        }

        public T DeleteReturnSingle<T>(string path)
        {
            return DeleteReturnAsOperationResult<T>(new BltHttpRequest { RequestUrl = path }).Result;
        }

        public T DeleteReturnSingle<T>(string path, object values, bool token = true)
        {
            return DeleteReturnAsOperationResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token).Result;
        }


        public OperationListResult<T> DeleteReturnAsListResult<T>(BltHttpRequest request, bool token = true)
        {
            request.RequestMethod = BltHttpMethod.DELETE;
            var httpResult = Do(request, token: token);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                var result = Activator.CreateInstance<OperationListResult<T>>();
                result.SetError(httpResult.StatusDescription);
                return result;
            }

            try
            {
                var result = JsonConvert.DeserializeObject<OperationListResult<T>>(httpResult.ContentBody ?? "");
                return result;
            }
            catch (Exception ex)
            {
                var result = Activator.CreateInstance<OperationListResult<T>>();
                result.SetError(ex.Message);
                return result;
            }

        }

        public OperationResult<T> DeletetReturnAsOperationResult<T>(string path, object values, bool token = true)
        {
            return DeleteReturnAsOperationResult<T>(new BltHttpRequest { RequestUrl = path, RequestBody = values }, token: token);
        }

        public T Delete<T>(BltHttpRequest request, bool token = true)
        {
            request.RequestMethod = BltHttpMethod.DELETE;
            var httpResult = Do(request, token: token);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                return Activator.CreateInstance<T>();
            }

            try
            {
                var result = JsonConvert.DeserializeObject<T>(httpResult.ContentBody ?? "");
                return result;
            }
            catch (Exception)
            {
                return Activator.CreateInstance<T>();
            }

        }

        public void Delete(BltHttpRequest request, bool token = true)
        {
            request.RequestMethod = BltHttpMethod.DELETE;
            var httpResult = Do(request, token: token);
            return;
        }

        public OperationResult<T> DeleteReturnAsOperationResult<T>(BltHttpRequest request, bool token = true)
        {
            request.RequestMethod = BltHttpMethod.DELETE;
            var httpResult = Do(request, token: token);

            if (httpResult.WebExceptionStatus.HasValue)
            {
                var result = Activator.CreateInstance<OperationResult<T>>();
                result.SetError(httpResult.StatusDescription);
                return result;
            }

            try
            {
                var result = JsonConvert.DeserializeObject<OperationResult<T>>(httpResult.ContentBody ?? "");
                return result;
            }
            catch (Exception ex)
            {
                var result = Activator.CreateInstance<OperationResult<T>>();
                result.SetError(ex.Message);
                return result;
            }
        }

        public BltHttpResponse GetRaw(BltHttpRequest request)
        {
            request.RequestMethod = BltHttpMethod.GET;
            return Do(request);
        }

        public BltHttpResponse PostRaw(BltHttpRequest request)
        {
            request.RequestMethod = BltHttpMethod.POST;
            return Do(request);
        }

        public BltHttpResponse PutRaw(BltHttpRequest request)
        {
            request.RequestMethod = BltHttpMethod.PUT;
            return Do(request);
        }

        public BltHttpResponse DeleteRaw(BltHttpRequest request)
        {
            request.RequestMethod = BltHttpMethod.DELETE;
            return Do(request);
        }

        public BltHttpResponse Do(BltHttpRequest request, int tryingCount = 1, bool token = true)
        {
            if (token)
            {
                if (Token == null) { GetToken(); }
            }

            BltHttpResponse result = new BltHttpResponse();
            result.HttpStatusCode = 500;
            result.StatusDescription = "Connection could not created";

            if (!request.RequestUrl.Contains(apiBasePath))
            {
                request.RequestUrl = String.Concat(apiBasePath, "/", request.RequestUrl.TrimStart('/'));
            }

            if (tryingCount > tryLimit)
            {
                result.StatusDescription = "trying limit has been exceeded :tried  " + tryingCount + " times";
                return result;
            }

            result = DoHttpRequest(request);

            while (result.HttpStatusCode == 401 && tryingCount < tryLimit)
            {
                var newToken = GetToken();
                tryingCount++;
                result = Do(request, tryingCount);
                if (result.HttpStatusCode != 401)
                {
                    return result;
                }
            }

            return result;
        }


        public BltHttpResponse DoOther(BltHttpRequest request, int tryingCount = 1, bool token = true)
        {
            // other işlemleri için kullanılacak
            if (token)
            {
                if (Token == null) { GetToken(); }
            }

            BltHttpResponse result = new BltHttpResponse();
            result.HttpStatusCode = 500;
            result.StatusDescription = "Connection could not created";

            if (!request.RequestUrl.Contains(OtherApiBasePath))
            {
                request.RequestUrl = String.Concat(OtherApiBasePath, "/", request.RequestUrl.TrimStart('/'));
            }

            if (tryingCount > tryLimit)
            {
                result.StatusDescription = "trying limit has been exceeded :tried  " + tryingCount + " times";
                return result;
            }

            result = DoHttpRequestOther(request);

            while (result.HttpStatusCode == 401 && tryingCount < tryLimit)
            {
                var newToken = GetToken();
                tryingCount++;
                result = DoOther(request, tryingCount);
                if (result.HttpStatusCode != 401)
                {
                    return result;
                }
            }

            return result;
        }

        public BltApiUser GetToken(string username = null, string password = null)
        {
            if (string.IsNullOrWhiteSpace(username) && string.IsNullOrWhiteSpace(password)) return null;

            BltHttpRequest request = new BltHttpRequest();
            request.RequestMethod = BltHttpMethod.POST;
            request.RequestUrl = String.Concat(apiBasePath, tokenPath);
            request.RequestBody = new { username = username ?? apiUsername, password = password ?? apiPassword };

            var response = DoHttpRequest(request);

            if (!String.IsNullOrWhiteSpace(response.ContentBody))
            {
                var httpResult = JsonConvert.DeserializeObject<BltApiUser>(response.ContentBody);

                if (httpResult != null && !String.IsNullOrWhiteSpace(httpResult.Token))
                {
                    Token = httpResult.Token;
                    TokenExpireUtc = httpResult.TokenExpiresAt;
                    curUser = httpResult;
                }
            }

            return curUser;
        }

        private BltHttpResponse DoHttpRequest(BltHttpRequest request)
        {
            BltHttpResponse result = new BltHttpResponse();
            result.HttpStatusCode = 500;
            result.StatusDescription = "Connection could not created";

            try
            {
                string url = request.RequestUrl;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                httpWebRequest.Method = request.RequestMethod.ToString();
                httpWebRequest.KeepAlive = true;
                httpWebRequest.Credentials = CredentialCache.DefaultCredentials;
                httpWebRequest.AllowAutoRedirect = true;
                httpWebRequest.Headers.Add("Authorization", "Bearer " + Token);
                httpWebRequest.Headers.Add("x-static-token", "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c");
                httpWebRequest.Headers.Add("x-ip", RequestExtender.GetIp());
                httpWebRequest.Headers.Add("x-browser", RequestExtender.GetBrowser());
                httpWebRequest.Headers.Add("x-os", RequestExtender.GetUserPlatform());
                httpWebRequest.Timeout = 10000000;

                //List<BltHeaderParam> param = new List<BltHeaderParam>();
                //param.Name = "x-static-token";
                //param.Value = "8aE9b56P3Bd8Yd043f8G09b3X83M4pH24Wf6L9Zc1NBf3c";
                //param.Name = "x-ip";
                //param.Value = RequestExtender.GetIp();
                //request.RequestHeaders.Add(param);

                //foreach (var item in request.RequestHeaders)
                //{
                //    httpWebRequest.Headers.Add(item.Name,item.Value);
                //}

                if (request.RequestMethod == BltHttpMethod.POST || request.RequestMethod == BltHttpMethod.PUT)
                {

                    httpWebRequest.ContentType = "application/json";
                    Stream memStream = new System.IO.MemoryStream();
                    string allParams = JsonConvert.SerializeObject(request.RequestBody);

                    byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(allParams);
                    memStream.Write(formitembytes, 0, formitembytes.Length);

                    Stream requestStream = httpWebRequest.GetRequestStream();

                    memStream.Position = 0;
                    byte[] tempBuffer = new byte[memStream.Length];
                    memStream.Read(tempBuffer, 0, tempBuffer.Length);
                    memStream.Close();
                    requestStream.Write(tempBuffer, 0, tempBuffer.Length);
                    requestStream.Close();

                }

                HttpWebResponse webResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                Stream responseStream = webResponse.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                result.ContentBody = reader.ReadToEnd();
                result.ContentType = webResponse.ContentType;
                result.HttpStatusCode = (int)webResponse.StatusCode;
                result.StatusDescription = webResponse.StatusDescription;

                webResponse.Close();
                httpWebRequest = null;
                webResponse = null;

                return result;

            }
            catch (WebException ex)
            {
                result.WebExceptionStatus = ex.Status;

                if (ex.Response != null)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        result.ContentBody = reader.ReadToEnd();
                        result.HttpStatusCode = Convert.ToInt32(((HttpWebResponse)ex.Response).StatusCode);
                        result.StatusDescription = ((HttpWebResponse)ex.Response).StatusDescription;
                    }
                }
                else
                {
                    result.HttpStatusCode = 500;
                    result.StatusDescription = ex.Message;
                }

            }
            catch (Exception ex)
            {
                result.ContentBody = String.Concat("Unknown Error on request", ex.Message);
                result.HttpStatusCode = 500;
                result.StatusDescription = result.ContentBody;
            }

            return result;
        }

        private BltHttpResponse DoHttpRequestOther(BltHttpRequest request)
        {
            //getCatalogue();
             // other işlemleri için kullanılacak
            BltHttpResponse result = new BltHttpResponse();
            result.HttpStatusCode = 500;
            result.StatusDescription = "Connection could not created";

            try
            {
                string url = request.RequestUrl;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                httpWebRequest.Method = request.RequestMethod.ToString();
                httpWebRequest.KeepAlive = true;
                httpWebRequest.Credentials = CredentialCache.DefaultCredentials;
                httpWebRequest.AllowAutoRedirect = true;
      

               

                HttpWebResponse webResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                Stream responseStream = webResponse.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                result.ContentBody = reader.ReadToEnd();
                result.ContentType = webResponse.ContentType;
                result.HttpStatusCode = (int)webResponse.StatusCode;
                result.StatusDescription = webResponse.StatusDescription;

                webResponse.Close();
                httpWebRequest = null;
                webResponse = null;

                return result;

            }
            catch (WebException ex)
            {
                result.WebExceptionStatus = ex.Status;

                if (ex.Response != null)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        result.ContentBody = reader.ReadToEnd();
                        result.HttpStatusCode = Convert.ToInt32(((HttpWebResponse)ex.Response).StatusCode);
                        result.StatusDescription = ((HttpWebResponse)ex.Response).StatusDescription;
                    }
                }
                else
                {
                    result.HttpStatusCode = 500;
                    result.StatusDescription = ex.Message;
                }

            }
            catch (Exception ex)
            {
                result.ContentBody = String.Concat("Unknown Error on request", ex.Message);
                result.HttpStatusCode = 500;
                result.StatusDescription = result.ContentBody;
            }

            return result;
        }

        //public void getCatalogue(CatalogueSearchViewModel ınputs)
        //{
             

        //    ınputs.searchType = "author";
        //    ınputs.searchKey = "ömer seyfettin";
        //    ınputs.homebranch = 29;
        //    ınputs.pagesize = 10;
        //    var model = JsonConvert.SerializeObject(ınputs);


        //    var client = new RestClient("http://koha-app.ekutuphane.gov.tr/api/catalogue/SearchCatalogue/");
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.POST);
        //    request.AddJsonBody(ınputs);
        //    request.AddParameter("application/json", "", ParameterType.RequestBody);
        //    request.AddHeader("Content-Type", "application/json");
        
        //    IRestResponse response = client.Execute(request);
        //    List<Catalogue> jsonArrayAttribute = JsonConvert.DeserializeObject<List<Catalogue>>(response.Content ?? "");
        //    Console.WriteLine(jsonArrayAttribute);

        // }
        
    }
}
