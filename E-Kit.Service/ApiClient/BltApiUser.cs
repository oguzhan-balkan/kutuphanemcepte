﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E_Kit.Service.ApiClient
{
    public class BltApiUser
    {
        public int Id { get; set; }
        public int CinemaBranchId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public DateTime TokenExpiresAt { get; set; }
        public List<string> Roles { get; set; }
        public BltInvalidLoginMessage message { get; set; }

        public class GeneralHttpResult<T>
        {
            public T Result { get; set; }
        }

        public class BltInvalidLoginMessage
        {
            public string Value { get; set; }
        }
    }
}
