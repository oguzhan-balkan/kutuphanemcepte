﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace E_Kit.Service.ApiClient
{
    public enum BltHttpMethod
    {
        GET = 0,
        POST = 1,
        PUT = 2,
        DELETE = 3
    }

    public class CacheRequestViewModel
    {
        public string key { get; set; }
        public string value { get; set; }
        public DateTime? expiresAt { get; set; }

    }

    public class BltHeaderParam
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class BltHttpRequest
    {
        public string RequestUrl { get; set; }
        public BltHttpMethod RequestMethod { get; set; }
        public object RequestBody { get; set; }
        public List<BltHeaderParam> RequestHeaders { get; set; } = new List<BltHeaderParam>();
    }

    public class BltHttpResponse
    {
        public string ContentBody { get; set; }
        public string ContentType { get; set; }
        public int HttpStatusCode { get; set; }
        public string StatusDescription { get; set; }
        public WebExceptionStatus? WebExceptionStatus { get; set; }

    }

    public class BltHttpResponseContent
    {
        public bool IsSuccess { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
    }
    public class OperationResult<T>
    {
        public void SetError(string message)
        {
            if (this.ResultDescription == null) this.ResultDescription = new List<string>();
            this.ResultDescription.Add(message);
        }

        public string GetResultDescriptionMessage()
        {
            if (this.ResultDescription != null && ResultDescription.Any())
            {
                var message = "";
                foreach (var item in ResultDescription.Where(c => c != null))
                {
                    message += item + " " + Environment.NewLine;
                }

                return message;
            }

            return "";
        }

        public List<string> ResultDescription { get; set; }
        public int ReturnCode { get; set; }
        public T Result { get; set; }
        public bool IsSuccess { get; set; }
    }

    public class OperationListResult<T>
    {
        public void SetError(string message)
        {
            if (this.ResultDescription == null) this.ResultDescription = new List<string>();
            this.ResultDescription.Add(message);
        }

        public List<string> ResultDescription { get; set; }

        public string GetResultDescriptionMessage()
        {
            if (this.ResultDescription != null && ResultDescription.Any())
            {
                var message = "";
                foreach (var item in ResultDescription.Where(c => c != null))
                {
                    message += item + " " + Environment.NewLine;
                }

                return message;
            }

            return "";
        }
        public int ReturnCode { get; set; }
        public List<T> Result { get; set; }
        public bool IsSuccess { get; set; }
    }

    public class StandardResult : OperationResult<object>
    {

    }
}
