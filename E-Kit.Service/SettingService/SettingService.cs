﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.SettingService
{
    public class SettingService : ISettingService
    {
        public bool EditSetting(Setting model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Setting?id={model.Id}", new { emailAddress = model.EmailAddress, emailPassword = model.EmailPassword,
             emailHost = model.EmailHost, emailPort = model.EmailPort, emailSsl = model.EmailSsl, socialFacebookUrl = model.SocialFacebookUrl, socialInstagramUrl = model.SocialInstagramUrl, 
                socialLinkedinUrl = model.SocialLinkedinUrl, socialTwitterUrl = model.SocialTwitterUrl, socialYoutubeUrl = model.SocialYoutubeUrl,
                defaultRentalTermDay = model.DefaultRentalTermDay,
                iosMaxVersion = model.IosMaxVersion,
                androidMaxVersion = model.AndroidMaxVersion,
                baseWebSiteUrl = model.BaseWebSiteUrl
            });
        }

        public Setting GetSetting()
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Setting>("/Setting/Get");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
