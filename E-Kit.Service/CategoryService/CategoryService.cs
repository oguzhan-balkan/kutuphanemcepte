﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.CategoryService
{
    public class CategoryService : ICategoryService
    {
        public bool DeleteCategory(int id)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Category?id={id}");
        }

        public bool EditCategory(Category model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Category?id={model.Id}", new { NameTr = model.NameTr, NameEn = model.NameEn, colorCode = model.ColorCode, isActive = model.IsActive });
        }

        public Category FindCategoryById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Category>($"/Category/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Category> GetCategoryList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Category>("/Category");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertCategory(Category model)
        {
            return new BltHttpClient().PostReturnSingle<bool>("Category", new { NameTr = model.NameTr, NameEn = model.NameEn, colorCode = model.ColorCode, isActive = model.IsActive });
        }

        public List<DropdownViewModel> GetCategorySelectList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<DropdownViewModel>("/Dropdown/Category");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
