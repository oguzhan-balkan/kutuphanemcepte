﻿using E_Kit.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.CategoryService
{
    public interface ICategoryService
    {
        List<Category> GetCategoryList();
        Category FindCategoryById(int id);
        bool InsertCategory(Category model);
        bool EditCategory(Category model);
        bool DeleteCategory(int id);
    }
}
