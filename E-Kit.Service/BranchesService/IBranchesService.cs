﻿using E_Kit.Data.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.BranchesService
{
    public interface IBranchesService
    {
        List<Branches> GetBranchesList();
        Branches FindBranchesById(int id);
        //bool InsertBranches(Branches model, MemoryStream data);
        //bool EditBranches(Branches model);
        bool DeleteBranches(int id);
    }
}
