﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using E_Kit.Service.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace E_Kit.Service.BranchesService
{
    public class BranchesService : IBranchesService
    {

        const string FOLDER_PATH_FOR = "/Files/";
        public bool DeleteBranches(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/Branches?id={id}");
        }

        //public bool EditBranches(Branches model)
        //{
        //    return new BltHttpClient().PutReturnSingle<bool>($"/Branches?id={model.Id}", new { titleTr = model.TitleTR, titleEn = model.TitleEN, textTr = model.TextTR,
        //    textEn = model.TextEN, image = model.ImageUrl, isActive = model.IsActive});
        //}

        public Branches FindBranchesById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Branches>($"/Branches/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Branches> GetBranchesList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResultOther<Branches>("/branches/GetAllBranches");

            try
            {

                return result;
                //if (result.IsSuccess == true)
                //{
                //    return result.Result;
                //}
                //else
                //{
                //    return null;
                //}
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //public bool InsertBranches(Branches model, MemoryStream data)
        //{
        //    return new BltHttpClient().PostReturnSingle<bool>("Branches", new {
        //        titleTr = model.TitleTR,
        //        titleEn = model.TitleEN,
        //        textTr = model.TextTR,
        //        textEn = model.TextEN,
        //        image = data,
        //        isActive = model.IsActive
        //    });
        //}

    }
}
