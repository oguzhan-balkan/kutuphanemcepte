﻿using E_Kit.Data.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.ChatService
{
    public interface IChatService
    {
        List<Chat> GetChatList();
        Chat FindChatById(string id);
        bool InsertChat(Chat model, MemoryStream data);
        //bool EditChat(Chat model);
        bool DeleteChat(int id);
        List<Chat> GetChatListName(string UserId);

    }
}
