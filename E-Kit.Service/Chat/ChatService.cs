﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using E_Kit.Service.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace E_Kit.Service.ChatService
{
    public class ChatService : IChatService
    {

        const string FOLDER_PATH_FOR = "/Files/";
        public bool DeleteChat(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/Chat?id={id}");
        }

        //public bool EditChat(Chat model)
        //{
        //    return new BltHttpClient().PutReturnSingle<bool>($"/Chat?id={model.Id}", new { userId = model.UserId, userName = model.UserName, branchCode = model.BranchCode,
        //    branchName = model.BranchName, isActive = model.IsActive});
        //}

        public Chat FindChatById(string id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Chat>($"/Chat/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Chat> GetChatList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Chat>("/Chat");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Chat> GetChatListName(string UserId)
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Chat>("/Chat/UserId?UserId="+UserId);

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertChat(Chat model, MemoryStream data)
        {
            return new BltHttpClient().PostReturnSingle<bool>("Chat", new {
                branchName = model.BranchName,
                branchCode = model.BranchCode,
                userId = model.UserId,
                userName = model.UserName,
            });
        }

    }
}
