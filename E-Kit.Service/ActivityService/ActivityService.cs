﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using E_Kit.Service.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace E_Kit.Service.ActivityService
{
    public class ActivityService : IActivityService
    {

        const string FOLDER_PATH_FOR = "/Files/";
        public bool DeleteActivity(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/Activity?id={id}");
        }

        public bool EditActivity(Activity model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Activity?id={model.Id}", new { titleTr = model.TitleTR, titleEn = model.TitleEN, textTr = model.TextTR,
            textEn = model.TextEN, image = model.ImageUrl, isActive = model.IsActive});
        }

        public Activity FindActivityById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Activity>($"/Activity/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Activity> GetActivityList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Activity>("/Activity");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result.OrderBy(p=>p.CreateDate).ToList();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertActivity(Activity model, MemoryStream data)
        {
            return new BltHttpClient().PostReturnSingle<bool>("Activity", new {
                titleTr = model.TitleTR,
                titleEn = model.TitleEN,
                textTr = model.TextTR,
                textEn = model.TextEN,
                image = data,
                isActive = model.IsActive
            });
        }

    }
}
