﻿using E_Kit.Data.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.ActivityService
{
    public interface IActivityService
    {
        List<Activity> GetActivityList();
        Activity FindActivityById(int id);
        bool InsertActivity(Activity model, MemoryStream data);
        bool EditActivity(Activity model);
        bool DeleteActivity(int id);
    }
}
