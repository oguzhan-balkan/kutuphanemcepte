﻿using E_Kit.Data.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.RoleService
{
    public interface IRoleService
    {
        List<Role> GetRoleList();
        Role FindRoleById(int id);
        bool InsertRole(Role model, MemoryStream data);
        bool EditRole(Role model);
        bool DeleteRole(int id);
    }
}
