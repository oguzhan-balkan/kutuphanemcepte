﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using E_Kit.Service.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace E_Kit.Service.RoleService
{
    public class RoleService : IRoleService
    {

        const string FOLDER_PATH_FOR = "/Files/";
        public bool DeleteRole(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/Role?id={id}");
        }

        public bool EditRole(Role model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Role?id={model.Id}", new { name = model.Name, isActive = model.IsActive});
        }

        public Role FindRoleById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Role>($"/Role/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Role> GetRoleList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Role>("/Role");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertRole(Role model, MemoryStream data)
        {
            return new BltHttpClient().PostReturnSingle<bool>("Role", new {
                
                name=model.Name,
                isActive = model.IsActive
            });
        }

    }
}
