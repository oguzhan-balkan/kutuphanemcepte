﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.UserService
{
    public class UserService : IUserService
    {
        public User FindByUserId(string id) 
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<User>($"/User/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public UserViewModel Login(string email, string password)
        {
            var client = new BltHttpClient();
            var result = client.Post<OperationResult<UserViewModel>>("/Auth/Login", new { email = email, password = password }, false);

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public UserViewModel ForgotPassword(string email)
        {
            var client = new BltHttpClient();
            var result = client.Post<OperationResult<UserViewModel>>("/Auth/PasswordForgot", new { email = email }, false);

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool EditUser(User model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/User?id={model.Id}", new
            {
                name = model.Name,
                lastName = model.LastName,
                email = model.Email,
                phoneNumber = model.PhoneNumber,
                isActive = model.IsActive
            });
        }

        public bool ChangePassword(ChangePasswordViewModel model)
        {
            var client = new BltHttpClient();
            

            return new BltHttpClient().PutReturnSingle<bool>($"/Auth/ChangePassword", new
            {
               password = model.Password
            });
        }

        public bool InsertUser(User model)
        {
            return new BltHttpClient().PostReturnSingle<bool>("User", new { name = model.Name, lastName = model.LastName, phoneNumber = model.PhoneNumber, email = model.Email, userGroupId = model.UserGroupId, isActive = model.IsActive });
        }

        public List<User> GetUserList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<User>("/User");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool DeleteUser(string id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/User?id={id}");
        }


    }
}
