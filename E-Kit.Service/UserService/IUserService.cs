﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace E_Kit.Service.UserService
{
    public interface IUserService
    {
        UserViewModel Login(string email, string password);
        UserViewModel ForgotPassword(string email);
        User FindByUserId(string id);
        bool EditUser(User model);
        bool ChangePassword(ChangePasswordViewModel model);
        bool InsertUser(User model);
        List<User> GetUserList();
        bool DeleteUser(string id);

    }
}
