﻿using E_Kit.Data.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.StoryService
{
    public interface IStoryService
    {
        List<Story> GetStoryList();
        Story FindStoryById(int id);
        bool InsertStory(Story model, MemoryStream data);
        bool EditStory(Story model);
        bool DeleteStory(int id);
    }
}
