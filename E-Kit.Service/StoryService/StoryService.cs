﻿using E_Kit.Data.Entities;
using E_Kit.Service.StoryService;
using E_Kit.Service.ApiClient;
using E_Kit.Service.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace E_Kit.Service.StoryService
{
    public class StoryService : IStoryService
    {

        const string FOLDER_PATH_FOR = "/Files/";
        public bool DeleteStory(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/Story?id={id}");
        }

        public bool EditStory(Story model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Story?id={model.Id}", new { titleTr = model.TitleTR, titleEn = model.TitleEN, textTr = model.TextTR,
            textEn = model.TextEN, image = model.ImageUrl, isActive = model.IsActive});
        }

        public Story FindStoryById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Story>($"/Story/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Story> GetStoryList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Story>("/Story");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertStory(Story model, MemoryStream data)
        {
            return new BltHttpClient().PostReturnSingle<bool>("Story", new {
                titleTr = model.TitleTR,
                titleEn = model.TitleEN,
                textTr = model.TextTR,
                textEn = model.TextEN,
                image = data,
                isActive = model.IsActive
            });
        }

    }
}
