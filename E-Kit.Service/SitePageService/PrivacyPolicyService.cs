﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.SitePageService
{
    public class PrivacyPolicyService : IPrivacyPolicy
    {
        public bool EditPrivacyPolicy(PrivacyPolicy model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/PrivacyPolicy", new
            {
                textTr = model.TextTr,
                textEn = model.TextEn
            });
        }

        public PrivacyPolicy GetPrivacyPolicy()
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<PrivacyPolicy>($"PrivacyPolicy");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
