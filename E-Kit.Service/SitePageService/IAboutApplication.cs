﻿using E_Kit.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.SitePageService
{
    public interface IAboutApplication
    {
        AboutApplication GetAboutApplication();
        bool EditAboutApplication(AboutApplication model);
    }
}
