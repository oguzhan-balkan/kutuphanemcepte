﻿using E_Kit.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.SitePageService
{
    public interface IPrivacyPolicy
    {
        PrivacyPolicy GetPrivacyPolicy();
        bool EditPrivacyPolicy(PrivacyPolicy model);
    }
}
