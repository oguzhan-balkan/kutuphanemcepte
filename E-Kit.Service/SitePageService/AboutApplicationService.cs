﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.SitePageService
{
    public class AboutApplicationService : IAboutApplication
    {
        public bool EditAboutApplication(AboutApplication model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/AboutApplication", new
            {
                textTr = model.TextTr,
                textEn = model.TextEn
            });

        }

        public AboutApplication GetAboutApplication()
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<AboutApplication>($"AboutApplication");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
