﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.SitePageService
{
    public class ContactService : IContact
    {
        public bool EditContact(Contact model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Contact", new
            {
                Tel = model.Tel,
                Email = model.Email,
                Address = model.Address,
                Location = model.Location
            });
        }

        public Contact GetContact()
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Contact>($"Contact");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
