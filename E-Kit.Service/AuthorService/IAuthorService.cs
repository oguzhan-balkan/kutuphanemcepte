﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.AuthorService
{
    public interface IAuthorService
    {
        List<Author> GetAuthorList();
        Author FindAuthorById(int id);
        bool InsertAuthor(Author model);
        bool EditAuthor(Author model);
        bool DeleteAuthor(int id);
        List<DropdownViewModel> GetAuthorSelectList();
    }
}
