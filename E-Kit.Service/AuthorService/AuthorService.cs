﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.AuthorService
{
    public class AuthorService : IAuthorService
    {
        public bool DeleteAuthor(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/Author?id={id}");
        }

        public bool EditAuthor(Author model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Author?id={model.Id}", new { id = model.Id, name = model.Name, lastName = model.LastName, title = model.Title, AboutTr = model.AboutTr, AboutEn = model.AboutEn, isActive = model.IsActive });
        }

        public Author FindAuthorById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Author>($"/Author/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Author> GetAuthorList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Author>("/Author");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<DropdownViewModel> GetAuthorSelectList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<DropdownViewModel>("/Dropdown/Author");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertAuthor(Author model)
        {
            return new BltHttpClient().PostReturnSingle<bool>("Author", new { name = model.Name, lastName = model.LastName, title = model.Title, isActive = model.IsActive });
        }
    }
}
