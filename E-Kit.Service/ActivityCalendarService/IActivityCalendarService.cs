﻿using E_Kit.Data.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.ActivityCalendarService
{
    public interface IActivityCalendarService
    {
        List<ActivityCalendar> GetActivityCalendarList();
        ActivityCalendar FindActivityCalendarById(int id);
        bool InsertActivityCalendar(ActivityCalendar model, MemoryStream data);
        bool EditActivityCalendar(ActivityCalendar model);
        bool DeleteActivityCalendar(int id);
    }
}
