﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using E_Kit.Service.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace E_Kit.Service.ActivityCalendarService
{
    public class ActivityCalendarService : IActivityCalendarService
    {

        const string FOLDER_PATH_FOR = "/Files/";
        public bool DeleteActivityCalendar(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/ActivityCalendar?id={id}");
        }

        public bool EditActivityCalendar(ActivityCalendar model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/ActivityCalendar?id={model.Id}", new { titleTr = model.TitleTR, titleEn = model.TitleEN, textTr = model.TextTR,
            textEn = model.TextEN, image = model.ImageUrl, isActive = model.IsActive});
        }

        public ActivityCalendar FindActivityCalendarById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<ActivityCalendar>($"/ActivityCalendar/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ActivityCalendar> GetActivityCalendarList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<ActivityCalendar>("/ActivityCalendar");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertActivityCalendar(ActivityCalendar model, MemoryStream data)
        {
            return new BltHttpClient().PostReturnSingle<bool>("ActivityCalendar", new {
                titleTr = model.TitleTR,
                titleEn = model.TitleEN,
                textTr = model.TextTR,
                textEn = model.TextEN,
                image = data,
                isActive = model.IsActive
            });
        }

    }
}
