﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using E_Kit.Service.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace E_Kit.Service.ActivityCategoryService
{
    public class ActivityCategoryService : IActivityCategoryService
    {

        const string FOLDER_PATH_FOR = "/Files/";
        public bool DeleteActivityCategory(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/ActivityCategory?id={id}");
        }

        public bool EditActivityCategory(ActivityCategory model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/ActivityCategory?id={model.Id}", new { titleTr = model.TitleTR, titleEn = model.TitleEN});
        }

        public ActivityCategory FindActivityCategoryById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<ActivityCategory>($"/ActivityCategory/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ActivityCategory> GetActivityCategoryList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<ActivityCategory>("/ActivityCategory");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result.OrderBy(p=>p.CreateDate).ToList();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertActivityCategory(ActivityCategory model)
        {
            return new BltHttpClient().PostReturnSingle<bool>("ActivityCategory", new {
                titleTr = model.TitleTR,
                titleEn = model.TitleEN
              });
        }

    }
}
