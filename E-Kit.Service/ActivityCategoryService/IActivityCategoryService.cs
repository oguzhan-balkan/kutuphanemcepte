﻿using E_Kit.Data.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.ActivityCategoryService
{
    public interface IActivityCategoryService
    {
        List<ActivityCategory> GetActivityCategoryList();
        ActivityCategory FindActivityCategoryById(int id);
        bool InsertActivityCategory(ActivityCategory model);
        bool EditActivityCategory(ActivityCategory model);
        bool DeleteActivityCategory(int id);
    }
}
