﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.BookService
{
    public class BookService : IBookService
    {
        public bool DeleteBook(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/Book?id={id}");
        }

        public bool EditBook(Book model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Book?id={model.Id}", new
            {
                name = model.Name,
                publisherId = model.PublisherId,
                languageId = model.LanguageId,
                pageCount = model.PageCount,
                file = model.FileUrl,
                image = model.ImageUrl,
                publicationYear = model.PublicationYear,
                introductionTr = model.IntroductionTr,
                introductionEn = model.IntroductionEn,
                isbn = model.ISBN,
                expirationDate = model.ExpirationDate,
                isActive = model.IsActive,
                isOffline = model.IsOffline,
                isMultiUserRental = model.IsMultiUserRental,
                isExtensibleRental = model.IsExtensibleRental,
                isTermalRental = model.IsTermRental,
                rentalTermDay = model.RentalTermDay,
                authors = model.Authors,
                tags = model.Tags,
                categories = model.Categories
            });
        }

        public Book FindBookById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Book>($"Book/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Book> GetBookList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Book>("/Book");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<DropdownViewModel> GetBookSelectList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<DropdownViewModel>("/Dropdown/Book");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertBook(Book model)
        {
            return new BltHttpClient().PostReturnSingle<bool>("Book", new
            {
                Name = model.Name,
                PublisherId = model.PublisherId,
                LanguageId = model.LanguageId,
                PageCount = model.PageCount,
                Image = model.ImageUrl,
                PublicationYear = model.PublicationYear,
                IntroductionTr = model.IntroductionTr,
                IntroductionEn = model.IntroductionEn,
                ISBN = model.ISBN,
                ExpirationDate = model.ExpirationDate,
                IsActive = model.IsActive,
                IsOffline = model.IsOffline,
                IsMultiUserRental = model.IsMultiUserRental,
                IsExtensibleRental = model.IsExtensibleRental,
                IsTermalRental = model.IsTermRental,
                RentalTermDay = model.RentalTermDay,
                Authors = model.Authors,
                Tags = model.Tags,
                File = model.FileUrl,
                Categories = model.Categories
            });
        }
    }
}
