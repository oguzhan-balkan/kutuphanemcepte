﻿using E_Kit.Data.ViewModel;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.BookService
{
    public class EditorBookService : IEditorBookService
    {
        public bool DeleteEditorBook(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/EditorBook?id={id}");
        }

        public bool EditEditorBook(EditorBookViewModel model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/EditorBook?id={model.Id}", new
            {
                BookId = model.BookId,
                SortNo = model.SortNo      
            });
        }

        public EditorBookViewModel FindEditorBookById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<EditorBookViewModel>($"EditorBook/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<EditorBookViewModel> GetEditorBookList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<EditorBookViewModel>("EditorBook");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertEditorBook(EditorBookViewModel model)
        {
            return new BltHttpClient().PostReturnSingle<bool>("EditorBook", new
            {
                bookId = model.BookId,
                sortNo = model.SortNo
            });
        }
    }
}
