﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.BookService
{
    public interface IBookService
    {
        List<Book> GetBookList();
        Book FindBookById(int id);
        bool InsertBook(Book model);
        bool EditBook(Book model);
        bool DeleteBook(int id);
        List<DropdownViewModel> GetBookSelectList();
    }
}
