﻿using E_Kit.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.BookService
{
    public interface IEditorBookService
    {
        List<EditorBookViewModel> GetEditorBookList();
        EditorBookViewModel FindEditorBookById(int id);
        bool InsertEditorBook(EditorBookViewModel model);
        bool EditEditorBook(EditorBookViewModel model);
        bool DeleteEditorBook(int id);
    }
}
