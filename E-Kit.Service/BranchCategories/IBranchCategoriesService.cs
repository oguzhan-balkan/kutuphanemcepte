﻿using E_Kit.Data.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.BranchesService
{
    public interface IBranchCategoriesService
    {
        List<BranchCategories> GetBranchCategoriesList();
    }
}
