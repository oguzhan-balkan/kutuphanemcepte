﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using E_Kit.Service.BranchesService;
using E_Kit.Service.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace E_Kit.Service.BranchCategoriesService
{
    public class BranchCategoriesService : IBranchCategoriesService
    {

        const string FOLDER_PATH_FOR = "/Files/";
        public List<BranchCategories> GetBranchCategoriesList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResultOther<BranchCategories>("/branches/GetBranchCategoriesList");

            try
            {

                return result;
                //if (result.IsSuccess == true)
                //{
                //    return result.Result;
                //}
                //else
                //{
                //    return null;
                //}
            }
            catch (Exception ex)
            {
                return null;
            }
        }

 
    }
}
