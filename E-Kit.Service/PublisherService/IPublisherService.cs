﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.PublisherService
{
    public interface IPublisherService
    {
        List<Publisher> GetPublisherList();
        Publisher FindPublisherById(int id);
        bool InsertPublisher(Publisher model);
        bool EditPublisher(Publisher model);
        bool DeletePublisher(int id);
        List<DropdownViewModel> GetPublisherSelectList();
    }
}
