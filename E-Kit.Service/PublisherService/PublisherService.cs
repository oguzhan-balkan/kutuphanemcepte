﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.PublisherService
{
    public class PublisherService : IPublisherService
    {
        public bool DeletePublisher(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/Publisher?id={id}");
        }

        public bool EditPublisher(Publisher model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Publisher?id={model.Id}", new { name = model.Name, image = model.ImageUrl, aboutTr = model.AboutTr, aboutEn = model.AboutEn, isActive = model.IsActive });
        }

        public Publisher FindPublisherById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Publisher>($"/Publisher/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Publisher> GetPublisherList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Publisher>("/Publisher");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<DropdownViewModel> GetPublisherSelectList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<DropdownViewModel>("/Dropdown/Publisher");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertPublisher(Publisher model)
        {
            return new BltHttpClient().PostReturnSingle<bool>("Publisher", new { name = model.Name, image = model.ImageUrl, aboutTr = model.AboutTr, aboutEn = model.AboutEn, isActive = model.IsActive });
        }
    }
}
