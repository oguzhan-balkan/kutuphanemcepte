﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using E_Kit.Service.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace E_Kit.Service.UserRoleService
{
    public class UserRoleService : IUserRoleService
    {

        const string FOLDER_PATH_FOR = "/Files/";
        public bool DeleteUserRole(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/UserRole?id={id}");
        }

        public bool EditUserRole(UserRole model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/UserRole?id={model.Id}", new { roleId = model.RoleId, userId = model.UserId});
        }

        public UserRole FindUserRoleById(string id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<UserRole>($"/UserRole/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<UserRole> GetUserRoleList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<UserRole>("/UserRole");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertUserRole(UserRole model)
        {
            return new BltHttpClient().PostReturnSingle<bool>("UserRole", new {
                
                 roleId=model.RoleId,
                userId = model.UserId
            });
        }

    }
}
