﻿using E_Kit.Data.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.UserRoleService
{
    public interface IUserRoleService
    {
        List<UserRole> GetUserRoleList();
        UserRole FindUserRoleById(string id);
        bool InsertUserRole(UserRole model);
        bool EditUserRole(UserRole model);
        bool DeleteUserRole(int id);
    }
}
