﻿using E_Kit.Data.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.ChatUserListService
{
    public interface IChatUserListService
    {
        List<ChatUserList> GetChatUserListList();
        ChatUserList FindChatUserListById(string id);
        bool InsertChatUserList(ChatUserList model, MemoryStream data);
        bool EditChatUserList(ChatUserList model);
        bool DeleteChatUserList(int id);
    }
}
