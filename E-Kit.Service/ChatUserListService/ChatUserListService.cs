﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using E_Kit.Service.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace E_Kit.Service.ChatUserListService
{
    public class ChatUserListService : IChatUserListService
    {

        const string FOLDER_PATH_FOR = "/Files/";
        public bool DeleteChatUserList(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/ChatUserList?id={id}");
        }

        public bool EditChatUserList(ChatUserList model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/ChatUserList?id={model.Id}", new { userId = model.UserId, userName = model.UserName, branchCode = model.BranchCode,
            branchName = model.BranchName, isActive = model.IsActive});
        }

        public ChatUserList FindChatUserListById(string id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<ChatUserList>($"/ChatUserList/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ChatUserList> GetChatUserListList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<ChatUserList>("/ChatUserList");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertChatUserList(ChatUserList model, MemoryStream data)
        {
            return new BltHttpClient().PostReturnSingle<bool>("ChatUserList", new {
                branchName = model.BranchName,
                branchCode = model.BranchCode,
                userId = model.UserId,
                userName = model.UserName,
                isActive = model.IsActive
            });
        }

    }
}
