﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using E_Kit.Service.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.LanguageService
{
    public class LanguageService : ILanguageService
    {
        public bool DeleteLanguage(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/Language?id={id}");
        }

        public bool EditLanguage(Language model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Language?id={model.Id}", new { Name = model.Name, IsActive = model.IsActive });

        }

        public Language FindLanguageById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Language>($"/Language/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Language> GetLanguageList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Language>("/Language");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<DropdownViewModel> GetLanguageSelectList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<DropdownViewModel>("/Dropdown/Language");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertLanguage(Language model)
        {
            return new BltHttpClient().PostReturnSingle<bool>("Language", new { Name = model.Name, IsActive = model.IsActive });
        }
    }
}
