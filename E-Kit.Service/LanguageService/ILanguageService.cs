﻿using E_Kit.Data.Entities;
using E_Kit.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.LanguageService
{
    public interface ILanguageService
    {
        List<Language> GetLanguageList();
        Language FindLanguageById(int id);
        bool InsertLanguage(Language model);
        bool EditLanguage(Language model);
        bool DeleteLanguage(int id);
        List<DropdownViewModel> GetLanguageSelectList();
    }
}
