﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.Common
{
    public static class Crypto
    {
        public static string ToCreateMd5(this string text)
        {
            var md5 = new MD5CryptoServiceProvider();
            var btr = Encoding.UTF8.GetBytes(text);
            btr = md5.ComputeHash(btr);
            var sb = new StringBuilder();
            foreach (byte ba in btr)
            {
                sb.Append(ba.ToString("x2").ToLower());
            }
            return sb.ToString();
        }
    }
}
