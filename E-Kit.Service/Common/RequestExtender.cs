﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace E_Kit.Service.Common
{
    public static class RequestExtender
    {
        public static string GetIpBase(this HttpRequestBase request)
        {
            var ipList = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            var ip = !string.IsNullOrEmpty(ipList) ? ipList.Split(',')[0] : request.ServerVariables["REMOTE_ADDR"];
            if (ip.Equals("::1"))
                ip = "127.0.0.1";
            return ip;
        }

        public static string GetIp()
        {
            if (HttpContext.Current == null || HttpContext.Current.Request == null) return "127.0.0.1";
            return HttpContext.Current.Request.GetIp();
        }

        public static string GetIp(this HttpRequest request)
        {
            var ipList = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            var ip = !string.IsNullOrEmpty(ipList) ? ipList.Split(',')[0] : request.ServerVariables["REMOTE_ADDR"];
            if (ip.Equals("::1"))
                ip = "127.0.0.1";
            return ip;
        }

        public static string GetBrowser()
        {
            return HttpContext.Current.Request.Headers["User-Agent"].ToString();
        }

        public static string GetUserPlatform()
        {
            var ua = HttpContext.Current.Request.UserAgent;

            if (ua.Contains("Android"))
                return string.Format("Android {0}", GetMobileVersion(ua, "Android"));

            if (ua.Contains("iPad"))
                return string.Format("iPad OS {0}", GetMobileVersion(ua, "OS"));

            if (ua.Contains("iPhone"))
                return string.Format("iPhone OS {0}", GetMobileVersion(ua, "OS"));

            if (ua.Contains("Linux") && ua.Contains("KFAPWI"))
                return "Kindle Fire";

            if (ua.Contains("RIM Tablet") || (ua.Contains("BB") && ua.Contains("Mobile")))
                return "Black Berry";

            if (ua.Contains("Windows Phone"))
                return string.Format("Windows Phone {0}", GetMobileVersion(ua, "Windows Phone"));

            if (ua.Contains("Mac OS"))
                return "Mac OS";

            if (ua.Contains("Windows NT 5.1") || ua.Contains("Windows NT 5.2"))
                return "Windows XP";

            if (ua.Contains("Windows NT 6.0"))
                return "Windows Vista";

            if (ua.Contains("Windows NT 6.1"))
                return "Windows 7";

            if (ua.Contains("Windows NT 6.2"))
                return "Windows 8";

            if (ua.Contains("Windows NT 6.3"))
                return "Windows 8.1";

            if (ua.Contains("Windows NT 10"))
                return "Windows 10";

            //fallback to basic platform:
            return (ua.Contains("Mobile") ? " Mobile " : "");
        }

        public static string GetMobileVersion(string userAgent, string device)
        {
            var temp = userAgent.Substring(userAgent.IndexOf(device) + device.Length).TrimStart();
            var version = string.Empty;

            foreach (var character in temp)
            {
                var validCharacter = false;
                int test = 0;

                if (Int32.TryParse(character.ToString(), out test))
                {
                    version += character;
                    validCharacter = true;
                }

                if (character == '.' || character == '_')
                {
                    version += '.';
                    validCharacter = true;
                }

                if (validCharacter == false)
                    break;
            }

            return version;
        }
    }
}
