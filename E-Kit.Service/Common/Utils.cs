﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.Common
{
    public static class Utils
    {
        public static string FormatUrl(string Deger, bool HardSecured = true)
        {
            if (String.IsNullOrWhiteSpace(Deger)) return "";



            Deger = Deger.Replace(" & ", "-");
            Deger = Deger.Replace("&", "");
            Deger = Deger.Replace("  ", " ");
            Deger = Deger.Replace(" ", "-");
            Deger = Deger.Replace(":", "");
            Deger = Deger.Replace(",", "");
            Deger = Deger.Replace("'", "-");
            Deger = Deger.Replace("%20", "-");
            Deger = Deger.Replace("20%", "-");
            Deger = Deger.Replace("ş", "s");
            Deger = Deger.Replace("İ", "i");
            Deger = Deger.Replace("ı", "i");
            Deger = Deger.Replace("ö", "o");
            Deger = Deger.Replace("Ö", "O");
            Deger = Deger.Replace("Ş", "S");
            Deger = Deger.Replace("ç", "c");
            Deger = Deger.Replace("Ç", "c");
            Deger = Deger.Replace("ğ", "g");
            Deger = Deger.Replace("Ğ", "G");
            Deger = Deger.Replace("ü", "u");
            Deger = Deger.Replace("Ü", "U");
            Deger = Deger.Replace(".", "-");
            Deger = Deger.Replace("+", "-");
            Deger = Deger.Replace("/", "-");
            Deger = Deger.Replace("&", "");
            Deger = Deger.Replace("}", "");
            Deger = Deger.Replace("{", "");
            Deger = Deger.Replace("%", "");
            Deger = Deger.Replace("?", "");
            Deger = Deger.Replace("m²", "m2");


            if (String.IsNullOrWhiteSpace(Deger)) return "";

            if (HardSecured)
            {
                Deger = SecurityService.ClearSqlInjectionHard(Deger);
            }

            Deger = Deger.Replace("--", "-");

            return Deger;

        }
    }
}
