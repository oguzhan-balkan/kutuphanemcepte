﻿using BiletiniAl.YoneticiPanel.Service.CacheService;
using E_Kit.Data.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace E_Kit.Service.Common
{
    public static class SecurityService
    {
        public static IBltCacheClient cache = new BltCacheClientFactory().Cache;
        public static bool IsFloodRequest(UserActivityType activity, int maxCount, string extraMessage = "", bool notify = true, bool checkByCookie = true, bool checkByUser = true, bool checkbyToken = false, bool checkbyIp = true)
        {
            var result = false;

            //if (HttpContext.Current != null && HttpContext.Current.Request!=null && HttpContext.Current.Request.IsLocal) return result;

            IsDangerousRequest(true);

            //Check flood by CookieId

            if (checkbyToken && HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.Headers["Authorization"] != null)
            {
                var token = !String.IsNullOrWhiteSpace(HttpContext.Current.Request.Headers["Authorization"]) ? "-Token-" + HttpContext.Current.Request.Headers["Authorization"].Replace(" ", "") : "-Token-none-";
                token = token.ToCreateMd5();

                var floodCheckToken = !String.IsNullOrWhiteSpace(HttpContext.Current.Request.Headers["Authorization"]) && IsFloodRequestBasedOnKey(activity, maxCount, token);
                if (floodCheckToken) { result = true; }
            }


            //Check flood by IP
            if (!result && checkbyIp)
            {
                var floodCheckIp = IsFloodRequestBasedOnKey(activity, maxCount, "-Ip-" + RequestExtender.GetIp());
                if (floodCheckIp) { result = true; }
            }

            return result;
        }

        private static bool IsFloodRequestBasedOnKey(UserActivityType activity, int maxCount, string floodKey)
        {
            string cacheKey = "flood-" + activity.ToString() + floodKey;

            var curCount = 1;
            var cachedValue = cache.Get<object>(cacheKey);

            if (cachedValue == null)
            {
                cache.Set(cacheKey, curCount, DateTime.Now.AddHours(1));
            }
            else
            {
                curCount = Convert.ToInt16(cachedValue);
                curCount++;
                cache.Set(cacheKey, curCount, DateTime.Now.AddHours(1));
            }

            if (curCount > maxCount)
            {
                return true;
            }

            return false;
        }

        public static bool IsDangerousRequest(bool notify = false)
        {
            var result = false;

            if (HttpContext.Current != null)
            {
                try
                {
                    foreach (var key in HttpContext.Current.Request.Form.AllKeys)
                    {
                        if (key.Contains("RequestVerificationToken") || key.ToLower().Contains("requestverificationtoken")) continue;

                        var isDangerous = HttpContext.Current.Request[key].hasDangerousWord();

                        if (key.ToLower() == "password") continue;
                        if (key.ToLower().Contains("card")) continue;
                        if (key.ToLower().Contains("iday")) continue;
                        if (key.ToLower().Contains("idyil")) continue;


                        if (isDangerous && notify)
                        {
                            var ip = RequestExtender.GetIp();
                            return true;
                        }
                    }

                    foreach (var key in HttpContext.Current.Request.QueryString.AllKeys)
                    {
                        if (key.Contains("RequestVerificationToken") || key.ToLower().Contains("requestverificationtoken")) continue;

                        var isDangerous = HttpContext.Current.Request[key].hasDangerousWord();

                        if (key.ToLower() == "password") continue;
                        if (key.ToLower().Contains("card")) continue;
                        if (key.ToLower().Contains("iday")) continue;
                        if (key.ToLower().Contains("idyil")) continue;


                        if (isDangerous && notify)
                        {
                            var ip = RequestExtender.GetIp();
                            return true;
                        }
                    }

                }
                catch
                {


                }
            }

            return result;
        }



        public static List<string> veryDangerousWords = new List<string> { "~", "xp_enumgroups", "xp_grantlogin", "xp_logevent", "xp_loginconfig", "xp_logininfo", "xp_msver", "xp_revokelogin", "xp_sprintf", "xp_sqlmaint", "xp_sscanf", "COLLATE", "information_schema", "20xp_cmdshell", "ASCII", "LOAD_FILE", "xp_cmdshell", "c:\\", "HEXNUMBER", "replace", "sp_executesql", "DELETE", "CREATE", "HAVING", "truncate", "script", "UPDATE", "DECLARE", "xp_dirtree", "varchar", "OPENROWSET", "SELECT", "SQLOLEDB" };

        public static bool hasDangerousWord(this string text)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(text)) return false;

                foreach (var item in veryDangerousWords)
                {
                    if (text.IndexOf(item, 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public const string ENCRIPTON_KEY = "SWMRX6SPBNI33277";

        public static string ClearSqlInjection(string text)
        {
            if (String.IsNullOrWhiteSpace(text)) return "";

            text = text.Replace(">", ""); // SQL injection temizleme fonksiyonumuz
            text = text.Replace("<", "");
            text = text.Replace("--", "");
            text = text.Replace("'", "");
            text = text.Replace("char ", "");
            text = text.Replace("delete ", "");
            text = text.Replace("insert ", "");
            text = text.Replace("update ", "");
            text = text.Replace("select ", "");
            text = text.Replace("truncate ", "");
            text = text.Replace("union ", "");
            text = text.Replace("script ", "");
            text = text.Replace("1=1", "");
            text = text.Replace("' OR ''='", "");
            text = text.Replace(";", "");
            text = text.Replace("(", "");
            text = text.Replace(")", "");
            text = text.Replace("[", "");
            text = text.Replace("]", "");
            text = text.Replace("=", "");
            text = text.Replace("delay", "");
            text = text.Replace("DELAY", "");
            text = text.Replace("WAITFOR", "");
            text = text.Replace("waitfor", "");
            text = text.Replace("++", "");
            text = text.Replace("%", "");
            return text;
        }

        public static string ClearSqlInjectionHard(string text)
        {
            if (String.IsNullOrWhiteSpace(text)) return "";

            text = ClearSqlInjection(text);

            text = Regex.Replace(text, ",", "");
            text = Regex.Replace(text, "/", "");
            text = Regex.Replace(text, "\n", "");
            text = Regex.Replace(text, "/?", "");
            text = Regex.Replace(text, "/*", "");
            text = Regex.Replace(text, "'", "");
            text = Regex.Replace(text, "&", "");
            text = Regex.Replace(text, "<", "");
            text = Regex.Replace(text, ">", "");
            text = Regex.Replace(text, "=", "");
            text = Regex.Replace(text, "%", "[%]");
            text = Regex.Replace(text, "--", "");
            text = Regex.Replace(text, ";", "");
            text = Regex.Replace(text, "AND", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "OR", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "LIKE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "JOIN", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "UNION", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "UPDATE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "SELECT", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "INSERT", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "İNSERT", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "CREATE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "DELETE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "TABLE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "DATABASE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "DROP", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "ALTER", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "HAVING", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "GROUP", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "BY", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "BETWEEN", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "IN", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "INNER", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "JOİN", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "SUM", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "SET", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "İNNER", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "İN", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "HAVİNG", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "LİKE", "", RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "UNİON", "", RegexOptions.IgnoreCase);
            return text;

        }

    }
}
