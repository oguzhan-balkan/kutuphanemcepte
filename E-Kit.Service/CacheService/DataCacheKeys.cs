﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiletiniAl.YoneticiPanel.Service.CacheService
{
    public static class DataCacheKeys
    {
        public const string HEADER_MENU_ITEMS = "HeaderMenuItems";
        public const string FOOTER_MENU_ITEMS = "FooterMenuItems";

        public const string FILTER_DATA = "FilterData";
        public const string ALL_SEARCH_ITEMS = "AllSearchItems";

    }
}
