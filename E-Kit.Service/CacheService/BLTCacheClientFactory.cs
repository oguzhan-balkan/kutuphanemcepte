﻿using BiletiniAl.YoneticiPanel.Service.CacheService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiletiniAl.YoneticiPanel.Service.CacheService
{

    public class BltCacheClientFactory
    {
        private IBltCacheClient cacheClient = null;

        public IBltCacheClient Cache
        {
            get
            {
                if (cacheClient != null)
                    return cacheClient;

                return CacheGet("InMemory");
            }
        }

        public IBltCacheClient CacheGet(string key)
        {

            //if (cacheClient != null)
            //    return cacheClient;
            if(String.IsNullOrWhiteSpace(key))
            {
                cacheClient = new InProcCacheClient();
                return cacheClient;
            }

            switch (key.ToUpper(new System.Globalization.CultureInfo("en")))
            {

                case "INMEMORY":
                    cacheClient = new InProcCacheClient();
                    break;
                default:
                    cacheClient = new InProcCacheClient();
                    break;

            }

            return cacheClient;
        }

    }


}
