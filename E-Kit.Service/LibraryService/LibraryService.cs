﻿using E_Kit.Data.Entities;
using E_Kit.Service.ApiClient;
using E_Kit.Service.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace E_Kit.Service.LibraryService
{
    public class LibraryService : ILibraryService
    {

        const string FOLDER_PATH_FOR = "/Files/";
        public bool DeleteLibrary(int id)
        {
            return new BltHttpClient().DeleteReturnSingle<bool>($"/Library?id={id}");
        }

        public bool EditLibrary(Library model)
        {
            return new BltHttpClient().PutReturnSingle<bool>($"/Library?id={model.Id}", new {image = model.ImageUrl, isActive = model.IsActive});
        }

        public Library FindLibraryById(int id)
        {
            var client = new BltHttpClient();
            var result = client.GetAsOperationResult<Library>($"/Library/{id}");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Library> GetLibraryList()
        {
            var client = new BltHttpClient();
            var result = client.GetAsListResult<Library>("/Library");

            try
            {
                if (result.IsSuccess == true)
                {
                    return result.Result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertLibrary(Library model, MemoryStream data)
        {
            return new BltHttpClient().PostReturnSingle<bool>("Library", new {
                   
                 image = data,
                isActive = model.IsActive
            });
        }

    }
}
