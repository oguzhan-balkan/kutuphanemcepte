﻿using E_Kit.Data.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E_Kit.Service.LibraryService
{
    public interface ILibraryService
    {
        List<Library> GetLibraryList();
        Library FindLibraryById(int id);
        bool InsertLibrary(Library model, MemoryStream data);
        bool EditLibrary(Library model);
        bool DeleteLibrary(int id);
    }
}
